﻿using Goldenblock.Common;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;

namespace Goldenblock.Controllers
{
    public class MemberManagementController : Controller
    {
        
        #region Methods
        public ActionResult CoinDepositAndWithdrawal()
        {
            return View();
        }
        public ActionResult CoinStaking()
        {
            return View();
        }
        public ActionResult CoinTransactionHistory()
        {
            return View();
        }
        public ActionResult UserProfile()
        {
            return View();
        }
        public ActionResult UploadProperty()
        {
            return View();
        }
        public ActionResult UserInfo()
        {
            return View();
        }
        #endregion
    }
}