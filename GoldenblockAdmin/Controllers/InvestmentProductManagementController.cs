﻿using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static GoldenblockAdmin.MvcApplication;

namespace Goldenblock.Controllers
{
    public class InvestmentProductManagementController : Controller
    {
        private readonly AbstractLookupConstructionPeriodServices abstractLookupConstructionPeriodServices;
        private readonly AbstractLookupReferenceDateServices abstractLookupReferenceDateServices;
        private readonly AbstractPropertyServices abstractPropertyServices;
        public InvestmentProductManagementController(AbstractLookupConstructionPeriodServices abstractLookupConstructionPeriodServices, AbstractLookupReferenceDateServices abstractLookupReferenceDateServices, AbstractPropertyServices abstractPropertyServices)
        {
            this.abstractLookupConstructionPeriodServices = abstractLookupConstructionPeriodServices;
            this.abstractLookupReferenceDateServices = abstractLookupReferenceDateServices;
            this.abstractPropertyServices = abstractPropertyServices;
        }


        #region Methods
        [NoDirectAccess]
        public ActionResult ProductInquiry()
        {
            return View();
        }

        [NoDirectAccess]
        public ActionResult InvestmentAttractionList()
        {
            return View();
        }

        public ActionResult UploadProperty()
        {
            ViewBag.ConstructionPeriod = BindConstructionPeriodDrp();
            ViewBag.ReferenceDate = BindReferenceDatedDrp();           
            return View();
        }             
            
        public ActionResult PropertyEdit(string PropertyId = "")
        {
            ViewBag.ConstructionPeriod = BindConstructionPeriodDrp();
            ViewBag.ReferenceDate = BindReferenceDatedDrp();
            ViewBag.PropertyId = ConvertTo.Integer(ConvertTo.Base64Decode(PropertyId));
            var result = abstractPropertyServices.Property_ById(ViewBag.PropertyId);
            return View(result.Item);
        }

        public ActionResult PropertyDetails()
        {
            return View();
        }
        #endregion

        public IList<SelectListItem> BindConstructionPeriodDrp()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                
                var models = abstractLookupConstructionPeriodServices.LookupConstructionPeriod_All(pageParam, ""); 

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
                return items;
            }
            catch (Exception e)
            {
                return items;
            }
        }

        public IList<SelectListItem> BindReferenceDatedDrp()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;


                var models = abstractLookupReferenceDateServices.LookupReferenceDate_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }
                return items;
            }
            catch (Exception e)
            {
                return items;
            }
        }

        [HttpPost]
        public JsonResult GetProperty(int Id)
        {
            var result = abstractPropertyServices.Property_ById(Id);            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddProperty(Property property)
        {  
            var result = abstractPropertyServices.Property_Upsert(property);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}