﻿using Goldenblock.Common;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;

namespace Goldenblock.Controllers
{
    public class NoticeManagementController : Controller
    {
        
        #region Methods
        public ActionResult Event()
        {
            return View();
        }
        public ActionResult EventDetails()
        {
            return View();
        }
        public ActionResult EventDetailsEdit()
        {
            return View();
        }
        public ActionResult Notice()
        {
            return View();
        }
        public ActionResult NoticeAdd()
        {
            return View();
        }
        public ActionResult NoticeDetails()
        {
            return View();
        }
        public ActionResult NoticeDetailsEdit()
        {
            return View();
        }
        public ActionResult Terms()
        {
            return View();
        }
        public ActionResult TermsAdd()
        {
            return View();
        }
        public ActionResult TermsDetails()
        {
            return View();
        }
        public ActionResult TermsDetailsEdit()
        {
            return View();
        }
        #endregion
    }
}