﻿using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Goldenblock.Controllers
{    
    public class AuthenticationController : Controller
    {
        //private readonly abstractadminservices abstractadminservices;
        //public authenticationcontroller(abstractadminservices abstractadminservices)
        //{
        //    this.abstractadminservices = abstractadminservices;
        //}


        #region Methods
        public ActionResult EmailVerification()
        {
            return View();
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        public ActionResult SignIn()
        {
            return View();
        }
        public ActionResult SignUp()
        {
            return View();
        }
        #endregion


        //[HttpPost]
        //public JsonResult AdminLogin(string Email, string Password)
        //{
        //    var result = abstractAdminServices.Admin_Login(Email, Password);
        //    if (result.Code == 200 && result.Item != null)
        //    {
        //        Session.Clear();
        //        ProjectSession.AdminId = result.Item.Id;
        //        HttpCookie cookie = new HttpCookie("AdminLogin");
        //        cookie.Values.Add("Id", result.Item.Id.ToString());

        //        cookie.Expires = DateTime.Now.AddDays(30);
        //        Response.Cookies.Add(cookie);
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
    }
}