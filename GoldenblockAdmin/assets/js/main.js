/*
* Goldan Block
* @version: 1.0.0
* @author: Uichamp
         * Copyright 2021 Uichamp
*/
(function () {
  'use strict';

  var tooltip = function () {
    $('[data-toggle="tooltip"]').tooltip();
  }();

  var popover = function () {
    $('[data-toggle="popover"]').popover();
  }();

  var productGallery = function () {
    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: true,
      centerMode: true,
      focusOnSelect: true
    }); // keeps thumbnails active when changing main image, via mouse/touch drag/swipe

    $('.slider-for').on('afterChange', function (event, slick, currentSlide, nextSlide) {
      //remove all active class
      $('.slider-nav .slick-slide').removeClass('slick-current'); //set active class for current slide

      $('.slider-nav .slick-slide:not(.slick-cloned)').eq(currentSlide).addClass('slick-current');
    });
  }();
})();