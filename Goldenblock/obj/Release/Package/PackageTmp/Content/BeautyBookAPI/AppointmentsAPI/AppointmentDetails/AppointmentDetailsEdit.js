﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var DeviceTokenNumber = getCookie("DeviceToken&Type");
var SalonId = getCookie("SalonId");

var getAppoinmentDetailsId = new URLSearchParams(window.location.search);
getAppoinmentDetailsId = parseInt(atob(getAppoinmentDetailsId.get('AppoinmentDetailsId')));
var getAppoinmentDetailsIdatob = ~~getAppoinmentDetailsId;

if (getAppoinmentDetailsIdatob > 0) {
    
    $('.EditStatustext').text('edit');
    $('.EditStatus').text('edit');
}

if (getAppoinmentDetailsIdatob == 0) {
    $('#appointmentForm').show();
}

//appoinmentDetails edit API call in ajax methos
function appoinmentDetailsedit() {
    

    if (getAppoinmentDetailsIdatob > 0) {

        $('#newCustomeradd').hide();

        $('#appointmentFormloader').show();
        $('#appointmentForm').hide();

        $("#userAssignto").html(``);
        $('#userAssignto').attr('disabled', true);

           
        $.ajax({
            type: 'POST',
            url: '' + APIEndPoint + '/api/userAppointments/UserAppointments_ById?Id=' + getAppoinmentDetailsIdatob + '',
            headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
            dataType: 'json',
            crossDomain: true,
            success: function (result) {
                

                var ServicesIdsarr = result.Item.ServicesIds;
                var ServicesIdsarrsplit = ServicesIdsarr.split(',');
                console.log(ServicesIdsarrsplit);

                $('#appointmentId').val(result.Item.Id)
                $('#appointmentsComment').val(result.Item.Comment);
                $('#appointmentsPrice').val(result.Item.Price);
                $('#appointmentsDuration').val(result.Item.Duration);
                $('#appointmentsDate').val(result.Item.AppointmentDate);
                $('#appointmentsTime').val(result.Item.AppointmentTime);
                $('#customerDatalist').selectpicker('val', result.Item.UserId);
                $('#categoryName').selectpicker('val', result.Item.CategoryId);
                $('#categoryName').val(result.Item.CategoryId);
                $('#categoryName').trigger('change');

                ServiceName();
                userAssignto();

                $('#userAssignto').selectpicker('val', result.Item.AssignedToUserId);
                $('#serviceName').selectpicker('val', ServicesIdsarrsplit);

                
                $('#appointmentFormloader').hide();
                $('#appointmentForm').show();

            }, error: function (error) {
                // Error function
                $('#appointmentFormloader').hide();
                $('#appointmentForm').show();
            }
        });
    }
    
    return false;
}