﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";

}

var DeviceTokenNumber = getCookie("DeviceToken&Type");
var SalonId = getCookie("SalonId");
var getAppoinmentDetailsId = new URLSearchParams(window.location.search);
getAppoinmentDetailsId = parseInt(atob(getAppoinmentDetailsId.get('AppoinmentDetailsId')));
var getAppoinmentDetailsIdatob = ~~getAppoinmentDetailsId;


//profile upload image name get
function profileUpload() {
    
    var fake_path = $('#customerPhoto').val();
    $('#uploadImagename').text(fake_path.split("\\").pop());
}

// Appoinment add api call in ajax methos
function appointmentsAdd() {
    

    $('#appointmentBtn').hide();
    $('#appointmentBtnloading').show();
    
    var AppointmentsAdd = new Object();
    AppointmentsAdd.Id = $('#appointmentId').val();
    AppointmentsAdd.UserId = $('#customerDatalist').val();
    AppointmentsAdd.SalonId = atob(SalonId);
    AppointmentsAdd.AssignedToUserId = $('#userAssignto').val();
    AppointmentsAdd.AppointmentDate = $('#appointmentsDate').val();
    AppointmentsAdd.AppointmentTime = $('#appointmentsTime').val();
    AppointmentsAdd.Price = $('#appointmentsPrice').val();
    AppointmentsAdd.Duration = $('#appointmentsDuration').val();
    AppointmentsAdd.Comment = $('#appointmentsComment').val();
    AppointmentsAdd.ServicesIds = $('#serviceName').val().toString();
    AppointmentsAdd.CategoryId = $("#categoryName").val();

    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/userAppointments/UserAppointments_Upsert',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(AppointmentsAdd),
        crossDomain: true,
        success: function (result) {
            
            if (result.Code == 200) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: result.Message,
                    showConfirmButton: false,
                    timer: 3000
                })
                setTimeout(function () {
                    window.location = "/Appointments/ManageAppointments";
                }, 3000);
            }

            $('#appointmentBtn').show();
            $('#appointmentBtnloading').hide();

        }, error: function (error) {
            
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: error.responseJSON.Message,
                showConfirmButton: false,
                timer: 3000
            })

            $('#appointmentBtn').show();
            $('#appointmentBtnloading').hide();
        }
    });
    return false;
}

//employeeTypedrp dropdown API call in ajax methos

function customerData() {
    
    var CustomerData = new Object();
    CustomerData.IsPageProvided = true;

    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/users/Users_All?search&LookUpUserTypeId=4&SalonId='+atob(SalonId)+'',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(CustomerData),
        crossDomain: true,
        async:false,
        success: function (result) {
            result.Values.reverse();
            $("#customerDatalist").html(``);
            
            for (i = 0; i < result.Values.length; i++) {
                $('#customerDatalist').append(`
                 <option value="${result.Values[i].Id}">${result.Values[i].UserName}  -  ${result.Values[i].Email}</option>
                `);
                $('.selectpicker').selectpicker("refresh");
            }

        }, error: function (error) {
            // Error function
        }
    });
    return false;
}


//CategoryName dropdown API call in ajax methos

function CategoryName() {

    let CategoryName = new Object();
    CategoryName.IsPageProvided = true;

    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/lookUpServices/LookUpServices_All?search&ParentId=0',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(CategoryName),
        crossDomain: true,
        async:false,
        success: function (result) {

            result.Values.reverse();

            $("#categoryName").html(``);

            for (i = 0; i < result.Values.length; i++) {
                $('#categoryName').append(`
                    <option value="${result.Values[i].Id}">${result.Values[i].Name}</option>
                `);
            }
            $('.selectpicker').selectpicker("refresh");

        }, error: function (error) {
            // Error function
        }
    });
    return false;
}


$('#categoryName').change(function () {
    ServiceName();
});


//ServiceName dropdown API call in ajax methos

function ServiceName() {

    $("#serviceName").html(``);
    $('#serviceName').attr('disabled' , true);

    let ServiceName = new Object();
    ServiceName.IsPageProvided = true;

    let serviceParentid = $('#categoryName').val();

    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/lookUpServices/LookUpServices_All?search&ParentId=' + serviceParentid + '',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(ServiceName),
        crossDomain: true,
        async:false,
        success: function (result) {

            result.Values.reverse();

            $("#serviceName").html(``);

            for (i = 0; i < result.Values.length; i++) {
                if (result.Values.length > 0) {
                    $('#serviceName').append(`
                        <option value="${result.Values[i].Id}">${result.Values[i].Name}</option>
                    `);
                    $('#serviceName').removeAttr("disabled");
                }
            }

            $('.selectpicker').selectpicker("refresh");


        }, error: function (error) {
            // Error function
        }
    });
    return false;
}

//ServiceName dropdown API call in ajax methos

function AppointmentsTime() {
    userAssignto();
}



function userAssignto() {
    
    $('#userAssignto').attr('disabled' , true);

    let UserAssignto = new Object();
    UserAssignto.IsPageProvided = true;

    let categoryId = $('#categoryName').val();
    var AppointmentsTime = $('#appointmentsTime').val();
    var AppointmentsDate = $('#appointmentsDate').val();
    var ServicesId = $('#serviceName').val().toString();

    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/userAppointments/UserAppointments_GetAssignTo?search&SalonId=' + atob(SalonId) + '&CategoryId=' + categoryId + '&ServicesIds=' + ServicesId + '&AppointmentDate=' + AppointmentsDate + '&AppointmentTime=' + AppointmentsTime + '&UserAppointmentId='+getAppoinmentDetailsIdatob+'',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(UserAssignto),
        crossDomain: true,
        async:false,
        success: function (result) {
            result.Values.reverse();
            $("#userAssignto").html(``);
            
            for (i = 0; i < result.Values.length; i++) {
                if (result.Values.length > 0) {
                    $('#userAssignto').append(`
                        <option data-value="${result.Values[i].TotalPrice},${result.Values[i].TotalDuration}" value="${result.Values[i].Id}">
                            ${result.Values[i].FirstName} ${result.Values[i].SecondName} -  ${result.Values[i].Email} - ${result.Values[i].LastAppointmentTime == '' || result.Values[i].LastAppointmentTime == null ? 'Available' : 'Next Appointment' + ' : ' + result.Values[i].LastAppointmentDate + '  ' + onTimeChange(result.Values[i].LastAppointmentTime)}
                        </option>
                    `);
                    $('#userAssignto').removeAttr("disabled");
                }
            }
            $('.selectpicker').selectpicker("refresh");

        }, error: function (error) {
            // Error function
        }
    });
    return false;
}

function getUserPriceDuration() {
    var PriceDuration = $('select#userAssignto').find(':selected').data('value');

    var PriceDurationSplit = PriceDuration.split(',');

    $('#appointmentsPrice').val(PriceDurationSplit[0]);
    $('#appointmentsDuration').val(PriceDurationSplit[1]);
}
