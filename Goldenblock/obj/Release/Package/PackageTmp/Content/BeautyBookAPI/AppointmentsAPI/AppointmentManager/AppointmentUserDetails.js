﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";

}

var DeviceTokenNumber = getCookie("DeviceToken&Type");
var SalonId = getCookie("SalonId");

function appoinmentUserId(AppoinmentUserdetails) {
    
    $('#profileModalbtn').html(``);
    $('#appointmentUserdetails').html(``);
    $('#appointmentUserdetailsloader').show();
    if (AppoinmentUserdetails > 0) {
        $.ajax({
            type: 'POST',
            url: '' + APIEndPoint + '/api/userAppointments/UserAppointments_ById?Id='+AppoinmentUserdetails+'',
            headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
            dataType: 'json',
            crossDomain: true,
            success: function (result) {

                if (result.Item.LookUpStatusId == 7 || result.Item.LookUpStatusId == 10) {
                    $('#profileModalbtn').hide();
                } else {
                    $('#profileModalbtn').show();
                }

                $('#appointmentUserdetails').append(`
                     <div class="col-lg-12">

                        <div class="row">
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Appointment ID</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.Id}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Customer Name</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.CustomerUsername == "" || result.Item.CustomerUsername == null ? '-' : result.Item.CustomerUsername}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Gender</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.CustomerGender == "" || result.Item.CustomerGender == null ? '-' : result.Item.CustomerGender}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Email</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.CustomerEmail == "" || result.Item.CustomerEmail == null ? '-' : result.Item.CustomerEmail}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Phone</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.CustomerPrimaryPhone == "" || result.Item.CustomerPrimaryPhone == null ? '-' : result.Item.CustomerPrimaryPhone}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Alternate Phone</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.CustomerAlternatePhone == "" || result.Item.CustomerAlternatePhone == null ? '-' : result.Item.CustomerAlternatePhone}</span>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Appointment Date</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.AppointmentDate == "" || result.Item.AppointmentDate == null ? '-' : onTimeChange(result.Item.AppointmentDate)}</span>
                                </div>
                            </div>


                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Appointment Time</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.AppointmentTime == "" || result.Item.AppointmentTime == null ? '-' : result.Item.AppointmentTime}</span>
                                </div>
                            </div>

                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Assign To</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.AssignedToUsername == "" || result.Item.AssignedToUsername == null ? '-' : result.Item.AssignedToUsername}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Duration</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.Duration == "" || result.Item.Duration == null ? '-' : result.Item.Duration} Min</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Price</div>
                                <div>
                                    <span class="font-weight-medium">$${result.Item.Price == "" || result.Item.Price == null ? '-' : result.Item.Price}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Services Taken</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.ServicesIdsName == "" || result.Item.ServicesIdsName == null ? '-' : result.Item.ServicesIdsName}</span>,
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <div class="text-gray-500 fs-13 mb-1">Comment</div>
                                <p>${result.Item.Comment == "" || result.Item.Comment == null ? '-' : result.Item.Comment}</p>
                            </div>
                        </div>
                    </div>

                `);

                 $('#profileModalbtn').append(`
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    ${result.Item.LookUpStatusId == 10 ? ``:`<a href="/Appointments/AppointmentDetails?AppoinmentDetailsId=${btoa(result.Item.Id)}" role="button" class="btn btn-primary">Edit</a>`}
                `);
                $('#appointmentUserdetailsloader').hide();
             
            }, error: function (error) {
                $('#appointmentUserdetailsloader').hide();
            }
        });
        return false;
    } else { }
}