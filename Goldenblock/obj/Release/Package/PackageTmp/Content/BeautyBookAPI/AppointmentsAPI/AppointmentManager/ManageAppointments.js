﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";

}

var DeviceTokenNumber = getCookie("DeviceToken&Type");
var SalonId = getCookie("SalonId");

// appoinmentListfillter funcation
function appoinmentListfillter() {
    $('#appoinmentSearch').hide();
    $('#appoinmentSearchloading').show();
    appoinmentList.init();
}

//resetappoinmentList funcation
function resetappoinmentList() {

    $('#appoinmentReset').hide();
    $('#appoinmentloading').show();

    $('#appoinmentDate').val('');
    $('#appoinmentTime').val('');
    $('#appoinmentStatus').val(null);
    $('#customerDatalist').val(0);
    $('#appoinmentAssignedTo').val(0);
    $('.selectpicker').selectpicker("refresh");
    appoinmentList.init();
}


//Appoinment list API call in ajax methos
var appoinmentList = function () {

    $('#appoinmentGridloader').show();

    debugger;
    let initappoinmentList = function () {


        let AppoinmentList = new Object();
        AppoinmentList.IsPageProvided = true;

        var appointmentDates = $('#appoinmentDate').val();
        var appoinmentTimes = $('#appoinmentTime').val();
        var appoinmentStatus = ~~$('#appoinmentStatus').val();
        var appoinmentCustomer = ~~$('#customerDatalist').val();
        var appoinmentAssignedToUser = ~~$('#appoinmentAssignedTo').val();

        $.ajax({
            processing: true,
            serverSide: true,
            type: 'POST',
            url: APIEndPoint + `/api/userAppointments/UserAppointments_All?search&SalonId=${atob(SalonId)}&CustomerId=${appoinmentCustomer}&AssignedToUserId=${appoinmentAssignedToUser}&AppointmentDate=${appointmentDates}&AppointmentTime=${appoinmentTimes}&LookUpStatusId=${appoinmentStatus}`,
            headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
            dataType: 'json',
            data: JSON.stringify(AppoinmentList),
            crossDomain: true,
            success: function (Values) {
                console.log(Values);
                debugger;
                $('#appoinmentGrid').DataTable({
                    "order": [[0, "desc"]],
                    data: Values.Values,

                    columns: [
                        {
                            "title": "ID", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `<a href="javascript:void(0)" onclick="appoinmentUserId(${row["Id"]})" data-toggle="modal" data-target="#appointmentModal" class="link">${row["Id"]}</a>`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "0%"
                        },
                        {
                            "title": "Customer Name", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `
                                    <span class="avatar avatar-primary avatar-circle">
                                        ${row["CustomerProfileUrl"] == "" || row["CustomerProfileUrl"] == null ?
                                        `<span class="avatar-initials">${row["CustomerFirstname"].charAt(0)} ${row["CustomerSecondName"].charAt(0)}</span>`
                                        :
                                        `<img src="${APIEndPoint}/${row["CustomerProfileUrl"]}" class="custome-profile-avatar" alt="User Profile"/>`
                                    }
                                    </span>
                                    <a href="javascript:void()" onclick="customerDetails(${row["UserId"]});"  class="link ml-2">${row["CustomerUsername"]}</a>
                                `;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "Gender", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["CustomerGender"] == "" || row["CustomerGender"] == null ? '-' : row["CustomerGender"]}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "2%"
                        },
                        {
                            "title": "Date", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["AppointmentDate"] == "" || row["AppointmentDate"] == null ? '-' : row["AppointmentDate"]}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "2%"
                        },
                        {
                            "title": "Time", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["AppointmentTime"] == "" || row["AppointmentTime"] == null ? '-' : onTimeChange(row["AppointmentTime"])}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "Services", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["ServicesIds"] == "" || row["ServicesIds"] == null ? '0' : row["ServicesIds"].split(',').length}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "0%"
                        },
                        {
                            "title": "Assign To", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `
                                    <span class="avatar avatar-primary avatar-circle">
                                        ${row["AssignedToProfile"] == "" || row["AssignedToProfile"] == null ?
                                        `<span class="avatar-initials">${row["AssignedToFirstname"].charAt(0)} ${row["AssignedToSecondName"].charAt(0)}</span>`
                                        :
                                        `<img src="${APIEndPoint}/${row["AssignedToProfile"]}" class="custome-profile-avatar" alt="User Profile"/>`
                                    }
                                    </span>
                                    <a data-toggle="modal" href="javascript:void()" onclick="appoinmentModaldetails(${row["AssignedToUserId"]});" data-target="#employeeModal"  class="link ml-2">${row["AssignedToUsername"]}</a>
                                `;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "Status", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData += `${row["LookUpStatusId"] == 6 ? `<div class="badge badge-warning p-2" style="cursor:pointer;">${row["LookUpStatusName"]}</div>` : ''}`;
                                htmlData += `${row["LookUpStatusId"] == 7 ? `<div class="badge badge-danger p-2" style="cursor:pointer;">${row["LookUpStatusName"]}</div>` : ''}`;
                                htmlData += `${row["LookUpStatusId"] == 8 ? `<div class="badge badge-warning p-2" style="cursor:pointer;">${row["LookUpStatusName"]}</div>` : ''}`;
                                htmlData += `${row["LookUpStatusId"] == 9 ? `<div class="badge badge-success p-2" style="cursor:pointer;">${row["LookUpStatusName"]}</div>` : ''}`;
                                htmlData += `${row["LookUpStatusId"] == 10 ? `<div class="badge badge-success p-2" style="cursor:pointer;">${row["LookUpStatusName"]}</div>` : ''}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "0%"
                        },
                        {
                            "title": "Actions", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `<div class="dropdown">
                                                <button class="btn btn-icon" type="button" data-toggle="dropdown" id="changeStatus">
                                                    <i class="bb-more-horizontal fs-22"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="javascript:void(0)" onclick="appoinmentUserId(${row["Id"]})" data-toggle="modal" data-target="#appointmentModal"><i class="bb-eye text-gray-600 fs-16 mr-3"></i>View</a>
                                                    ${row["LookUpStatusId "] == 7 || row["LookUpStatusId"] == 10 ? '' :
                                                        `<a class="dropdown-item" href="/Appointments/AppointmentDetails?AppoinmentDetailsId=${btoa(row["Id"])}"><i class="bb-edit-3 text-gray-600 fs-16 mr-3"></i>Edit</a>
                                                         <a class="dropdown-item" href="javascript:void(0)" onclick="changeStatus(${row["Id"]})"><i class="bb-repeat text-gray-600 fs-16 mr-3"></i>Change Status</a>`
                                                    }
                                                    <hr>
                                                    <a class="dropdown-item text-danger" onclick="DeleteAppoinmentManager(${row["Id"]})" href="javascript:void(0)"><i class="bb-trash-2 text-danger fs-16 mr-3"></i>Delete</a>
                                                </div>
                                            </div>`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "0%"
                        },
                        
                    ],
                    buttons: [
                        {
                            className: 'btn btn-primary float-left mb-3',
                            text: '<i class="bb-plus fs-16 mr-1"></i> Add New Appointments',
                            action: function (e, dt, node, config) {
                                window.location = '/Appointments/AppointmentDetails';
                            }
                        },
                        {
                            extend: 'pdf',
                            className: 'btn btn-light border font-weight-medium float-right mb-3',
                            text: '<i class="bb-printer fs-16 mr-2"></i>Print',
                        },
                        {
                            extend: 'excel',
                            className: 'btn btn-light border font-weight-medium float-right mb-3 mr-2',
                            text: '<i class="bb-download fs-16 mr-2"></i> Export to Excel',
                        },
                    ],
                    responsive: true,
                    "lengthMenu": [
                        [5, 15, 20, 40],
                        [5, 15, 20, 40] // change per page values here
                    ],
                    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                });


                $('#appoinmentGridloader').hide();

                $('#appoinmentSearch').show();
                $('#appoinmentSearchloading').hide();

                $('#appoinmentReset').show();
                $('#appoinmentloading').hide();

            }, error: function (error) {
                $('#appoinmentGridloader').hide();

                $('#appoinmentSearch').show();
                $('#appoinmentSearchloading').hide();

                $('#appoinmentReset').show();
                $('#appoinmentloading').hide();
            }
        });
    }
    return {
        //main function to initiate the module
        init: function () {
            if ($.fn.DataTable.isDataTable("#appoinmentGrid")) {
                $('#appoinmentGrid').dataTable().fnDestroy();
                $('#appoinmentGriddiv').html('<table id="appoinmentGrid" class="table table-card" style="width:100%; display:inherit;"></table >');
            }
            initappoinmentList();
        }
    };
}();

//appoinmentAssignedToUser dropdown API call in ajax methos

function appoinmentAssignedUser() {
    

    $("#appoinmentAssignedUser").html(``);

    let AppoinmentAssignedUser = new Object();
    AppoinmentAssignedUser.IsPageProvided = true;

    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/userAppointments/UserAppointments_GetAssignTo?search&SalonId=' + atob(SalonId) + '',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(AppoinmentAssignedUser),
        crossDomain: true,
        success: function (result) {
            result.Values.reverse();
            for (i = 0; i < result.Values.length; i++) {
                if (result.Values.length > 0) {
                    $('#appoinmentAssignedToUser').append(`
                        <option value="${result.Values[i].Id}">${result.Values[i].FirstName} ${result.Values[i].SecondName}</option>
                    `);
                    $('#userAssignto').removeAttr("disabled");
                    $('.selectpicker').selectpicker("refresh");
                }
            }
        }, error: function (error) {
            // Error function
        }
    });
    return false;
}


//employeeList API call in ajax method
function appoinmentAssignedUser() {
    $('#appoinmentAssignedToUser').html('');
    var employeeList = new Object();
    employeeList.IsPageProvided = true;
    
    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/users/Users_All?search&LookUpStatusId=0&LookUpUserTypeId=3&Name&LookUpEmployeeTypeId=0&LookUpEmployeeRolesId=0&SalonId=' + atob(SalonId) + '',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        crossDomain: true,
        data: JSON.stringify(employeeList),
        success: function (result) {
            for (i = 0; i < result.Values.length; i++) {
                if (result.Values.length > 0) {
                    $('#appoinmentAssignedTo').append(`
                        <option value="${result.Values[i].Id}">${result.Values[i].UserName} ${result.Values[i].SecondName} - ${result.Values[i].Email}</option>
                    `);
                    $('#userAssignto').removeAttr("disabled");
                    $('.selectpicker').selectpicker("refresh");
                }
            }
        }, error: function (error) {
            // Error function
        }
    });
    return false;
}



//appoinment api delete function

//swal Delete employee
function DeleteAppoinmentManager(employeeManagerId) {
    Swal.fire({
        title: 'Are you sure you want to delete this appoinment ?',
        //text: "Are you sure Active this employee !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.isConfirmed) {
            DeleteAppoinmentswal(employeeManagerId);
        }
    })
}

// DeleteEmployeeManager API call in ajax method
function DeleteAppoinmentswal(employeeManagerId) {
    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/userAppointments/UserAppointments_Delete?Id=' + employeeManagerId + '&DeletedBy=' + employeeManagerId+'',
        headers: { "Authorization": '' + DeviceTokenNumber + '' },
        processData: false,
        contentType: false,
        crossDomain: true,
        success: function (result) {
            
            if (result.Code == 200) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: result.Message,
                    showConfirmButton: false,
                    timer: 3000
                })
                appoinmentList.init();
            }
        }, error: function (error) {
            // Error function
        }
    });
}



