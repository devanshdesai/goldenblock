﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//setCookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

var DeviceTokenNumber = getCookie("DeviceToken&Type");
var salonId = getCookie("SalonId");

function CartProduct() {
    debugger;
    $('#productCartloader').show();

    var cartProduct = new Object();
    cartProduct.IsPageProvided = true;

    $.ajax({
        type: 'POST',
        url: APIEndPoint + `/api/addToCart/AddToCart_All?search&SalonId=${atob(salonId)}`,
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(cartProduct),
        crossDomain: true,
        success: function (result) {
            debugger;
            if (result.Values.length <= 0) {
                $('#productCart').html(`
                     <div class="text-center">
                        <h2 class="mb-0" style="font-size:18px;font-weight:500;">No records found</h2>
                     </div>
                `)
            } else {
                $("#productCart").html(``);
            }

            var SumTotalAmo = 0;
            var SumProductTax = 0;

            //cart product count 
            for (i = 0; i < result.Values.length; i++) {

                //Cart price sum in jquery use
                //SubTotal in cart
                SumTotalAmo += +result.Values[i].ProductTotalAmount;
                $('#subTotal').text('$' + `${SumTotalAmo}` + '.00');
                $('#subTotalInp').val(SumTotalAmo);

                //Sub Total in Vat Tax
                SumProductTax += +result.Values[i].ProductTax;
                $('#VATIncluded').text('$' + `${SumProductTax}` + '.00');
                $('#VATIncludedInp').val(SumProductTax);

                $('#TotaltoPay').text('$' + `${SumTotalAmo + SumProductTax}` + '.00');
                $('#TotaltoPayInp').val(SumTotalAmo + SumProductTax);



                $('#productCart').append(`
                    <div class="media">
                        <div class="avatar avatar-xl border overflow-hidden mr-3">
                            <img class="img-fluid" src="${APIEndPoint}/${result.Values[i].ProductThumbnailImage}" alt="Image Description">
                            <input type="hidden" id="productCartId" value="${result.Values[i].Id}"/>
                            <input type="hidden" id="productVendorId" value="${result.Values[i].VendorId}"/>
                        </div>

                        <div class="media-body">
                            <div class="row">
                                <div class="col-md-6 mb-3 mb-md-0">
                                    <a class="fs-16 link font-weight-medium d-block" href="/Store/ViewProduct?productId=${btoa(result.Values[i].ProductId)}">
                                        ${result.Values[i].ProductName}
                                    </a>
                                    <h5 class="font-weight-medium fs-14 mt-3">$${result.Values[i].ProductPrice}</h5>
                                </div>

                                <div class="col col-md-3 col-12 mb-1 align-self-center">
                                    <div class="store-touchspin">
                                        <input id="productQty" class="subtract-quantity text-center form-control-sm" type="text" value="${result.Values[i].Qty}"
                                                name="subtract-quantity">
                                    </div>
                                </div>

                                <div class="col col-md-3 col-12 mb-1 align-self-center text-right">
                                    <h5 class="font-weight-medium fs-14">$${result.Values[i].ProductTotalAmount}</h5>

                                    <span class="text-danger" role="button" onclick="deleteProductConfirm(${result.Values[i].Id});">
                                        <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                        </svg>
                                    </span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <hr class="hr-line">
                `);
            }
            //subtract-quantity function
            touchSpin();
            $('#productCartloader').hide();
        }, error: function (error) {
            // Error function
            $('#productCartloader').hide();
        }
    });
    
    return false;
}

//deleteProductconfirm function
function deleteProductConfirm(cartProductId) {

    Swal.fire({
        title: 'Are you sure you want to remove this product ?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.isConfirmed) {
            DeleteProductSwal(cartProductId);
        }
    })
}

//deleteProduct function

function DeleteProductSwal(cartProductId) {
    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/addToCart/AddToCart_Delete?Id=' + cartProductId + '&DeletedBy=' + atob(UserId) + '',
        headers: { "Authorization": '' + DeviceTokenNumber + '' },
        success: function (result) {
            if (result.Code == 200) {
                CartProduct();
                if (result.Code == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: result.Message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                }
            }
        }, error: function (error) {

            Swal.fire({
                position: 'center',
                icon: 'error',
                title: error.responseJSON.Message,
                showConfirmButton: false,
                timer: 3000
            })
        }
    });
}

