﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";

}

var DeviceTokenNumber = getCookie("DeviceToken&Type");
var SalonId = getCookie("SalonId");

var getVendorId = new URLSearchParams(window.location.search);
getVendorId = parseInt(atob(getVendorId.get('VendorId')));
var getVendorIdatob = ~~getVendorId;



//product list api call
function SalonOrderData() {

    $('#salonOrderdataloader').show();
    debugger;
    let OrderData = new Object();
    OrderData.IsPageProvided = true;

    debugger;
    $.ajax({
        type: 'POST',
        url: APIEndPoint + `/api/orders/Orders_All?search=&LookUpStatusId=0&OrderNo=&DateOfOrder=&SalonId=${atob(SalonId)}`,
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(OrderData),
        crossDomain: true,
        async: false,
        success: function (result) {
            debugger;
            if (result.Values.length <= 0) {
                $('#salonOrderdata').html(`
                    <tr>
                        <td colspan="100" class="text-center" style="font-size:18px;font-weight:500;">No records found</td>
                    </tr>
                `)
            } else {
                $("#salonOrderdata").html(``);
            }

            for (i = 0; i < result.Values.length; i++) {
                $('#salonOrderdata').append(`
                    <tr>
                        <th scope="row"><a href="javascript:;" onclick="OrderItemData(${result.Values[i].Id});">#${result.Values[i].Id}</a></th>
                        <td>${result.Values[i].OrderDate}</td>
                        <td>x${result.Values[i].TotalItems}</td>
                        <td>$${result.Values[i].TotalAmount}</td>
                    </tr>
                `);
            }

            $('#salonOrderdataloader').hide();

        }, error: function (error) {
            $('#salonOrderdataloader').hide();
            // Error function
        }
    });
    return false;
}


//Order Item Data api function call
function OrderItemData(orderId) {
    debugger;
    $('#orderDetailsModal').modal('show');
    let orderItemData = new Object();
    orderItemData.IsPageProvided = true;
    debugger;
    $.ajax({
        type: 'POST',
        url: APIEndPoint + `/api/orderProducts/OrderProducts_All?search=&OrderId=${orderId}&ProductId=0`,
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(orderItemData),
        crossDomain: true,
        async: false,
        success: function (result) {

            $('#OrderId').text('#'+orderId);

            debugger;
            if (result.Values.length <= 0) {
                $('#orderItem').html(`
                    <tr>
                        <td colspan="100" class="text-center" style="font-size:18px;font-weight:500;">No records found</td>
                    </tr>
                `)
            } else {
                $("#orderItem").html(``);
                $("#subTotalarea").html(``);
            }

            for (i = 0; i < result.Values.length; i++) {
                $('#orderItem').append(`
                    <tr>
                        <th scope="row">${result.Values[i].Id}</th>
                        <td>${result.Values[i].ProductName == "" || result.Values[i].ProductName == null ? '-' : result.Values[i].ProductName}</td>
                        <td>
                            ${result.Values[i].Weight == "" || result.Values[i].Weight == null ? '-' : result.Values[i].Weight}
                            ${result.Values[i].WeightTypeName == "" || result.Values[i].WeightTypeName == null ? '' : result.Values[i].WeightTypeName}
                        </td>
                        <td>$${result.Values[i].Rate == "" || result.Values[i].Rate == null ? '-' : result.Values[i].Rate}</td>
                        <td>x${result.Values[i].Qty == "" || result.Values[i].Qty == null ? '-' : result.Values[i].Qty }</td>
                        <td>$${result.Values[i].Total == "" || result.Values[i].Total == null ? '-' : result.Values[i].Total}</td>
                    </tr>
                `);
            }

            $('#subTotalarea').append(`
                <dl class="row text-sm-right">
                    <dt class="col-sm-6">Subtotal:</dt>
                    <dd class="col-sm-6">$${result.Values[0].SubTotal == "" || result.Values[0].SubTotal == null ? '-' : result.Values[0].SubTotal}.00</dd>
                    <dt class="col-sm-6">Shipping fee:</dt>
                    <dd class="col-sm-6">$0.00</dd>
                    <dt class="col-sm-6">Tax:</dt>
                    <dd class="col-sm-6">$${result.Values[0].Tax == "" || result.Values[0].Tax == null ? '-' : result.Values[0].Tax}.00</dd>
                    <dt class="col-sm-6">Total:</dt>
                    <dd class="col-sm-6">$${result.Values[0].SubTotalWithTax == "" || result.Values[0].SubTotalWithTax == null ? '-' : result.Values[0].SubTotalWithTax}.00</dd>
                </dl>
            `);

        }, error: function (error) {
            // Error function
        }
    });
    return false;

}