﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";

}

var DeviceTokenNumber = getCookie("DeviceToken&Type");
var SalonId = getCookie("SalonId");

var getSalonServicesId = new URLSearchParams(window.location.search);
getSalonServicesId = parseInt(atob(getSalonServicesId.get('SalonServices')));
var getSalonServicesIdatob = ~~getSalonServicesId;

//CategoryName dropdown API call in ajax methos

function CategoryName() {
    let CategoryName = new Object();
    CategoryName.IsPageProvided = true;

    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/lookUpServices/LookUpServices_All?search&ParentId=0',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(CategoryName),
        crossDomain: true,
        async:false,
        success: function (result) {
            result.Values.reverse();
            $("#categoryName").html(``);
          
            for (i = 0; i < result.Values.length; i++) {
                $('#categoryName').append(`
                    <option value="${result.Values[i].Id}">${result.Values[i].Name}</option>
                `);
                $('.selectpicker').selectpicker("refresh");
            }
        }, error: function (error) {
            // Error function
            
        }
    });
    return false;
}


$('#categoryName').change(function () {
    ServiceName();    
});

//ServiceName dropdown API call in ajax methos

function ServiceName() {

    $("#serviceName").html(``);
    $('#serviceName').attr('disabled', true);

    let ServiceName = new Object();
    ServiceName.IsPageProvided = true;
    let serviceParentid = $('#categoryName').val();
    
    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + '/api/lookUpServices/LookUpServices_All?search&ParentId=' + serviceParentid + '',
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        data: JSON.stringify(ServiceName),
        crossDomain: true,
        async:false,
        success: function (result) {
            result.Values.reverse();
            for (i = 0; i < result.Values.length; i++) {
                if (result.Values.length > 0) {
                    $('#serviceName').append(`
                        <option value="${result.Values[i].Id}">${result.Values[i].Name}</option>
                    `);
                    $('#serviceName').removeAttr("disabled");
                }
            }

            $('.selectpicker').selectpicker("refresh");

        }, error: function (error) {
            
        }
    });
    return false;
}

//employeeCharges section show condition

if (getSalonServicesIdatob > 0) {
    $('#employeeCharges').show();
}
else {
    $('#employeeCharges').html("");
}

//addSalonservices API Call In Ajax Method

function addSalonservices() {
    $('#addSalonservicesbtn').hide();
    $('#addSalonservicesbtnloading').show();
    
    let isValid = true;
    let errorMsg = "";
    let DurationStr = [];
    let PriceStr = [];
    let UserIdStr = [];
    if (EmployeeLength > 0) {
        for (i = 0; i < EmployeeLength; i++) {
            if (parseInt($(`#EmployeeData #emp${i} #empPrice`).val()) > 0 && parseInt($(`#EmployeeData #emp${i} #empDuration`).val()) > 0) {
                DurationStr.push($(`#EmployeeData #emp${i} #empDuration`).val());
                PriceStr.push($(`#EmployeeData #emp${i} #empPrice`).val());
                UserIdStr.push($(`#EmployeeData #emp${i} #UsId`).val());
                isValid = true;
            }
            else {
                isValid = false;
                errorMsg = "Please Enter Valid Price and Duration"
                break;
            }
        }
    }
    let AddSalonServices = new Object();
    if (isValid == true) {
        AddSalonServices.Id = $('#salonsServiceId').val();
        AddSalonServices.LookUpServicesId = $('#serviceName').val();
        if ($('#categoryName').val() != "") {
            AddSalonServices.LookUpCategoryId = $('#categoryName').val();
            
        } else {
            errorMsg = "Please Select Category";
            isValid = false;
        }
        
        AddSalonServices.SalonId = atob(SalonId);
        AddSalonServices.Description = $('#DescriptionComment').val();
        AddSalonServices.UserServiceId = UserIdStr.join(",");
        AddSalonServices.Duration = DurationStr.join(",");
        AddSalonServices.Price = PriceStr.join(",");
        AddSalonServices.CreatedBy = atob(UserId);
        AddSalonServices.UpdatedBy = atob(UserId);
        AddSalonServices.DeletedBy = atob(UserId);
    }
    
    if (isValid) {
        $.ajax({
            type: 'POST',
            url: '' + APIEndPoint + '/api/salonServices/SalonServices_Upsert',
            headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
            dataType: 'json',
            data: JSON.stringify(AddSalonServices),
            crossDomain: true,
            success: function (result) {
                
                if (result.Code == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: result.Message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                    setTimeout(function () {
                        window.location.href = '/SalonServices/SalonServiceDetails?SalonServices=' + btoa(result.Item.Id) + '';
                    }, 3000);
                }
                //$('#employeeForm')[0].reset();
                //$('.selectpicker').selectpicker("refresh");
                $('#addSalonservicesbtn').show();
                $('#addSalonservicesbtnloading').hide();

            }, error: function (error) {
                
                let msg = error.responseJSON.Message != null ? error.responseJSON.Message : error.responseJSON.Error.Message;
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: msg,
                    showConfirmButton: false,
                    timer: 3000
                });
                $('#addSalonservicesbtn').show();
                $('#addSalonservicesbtnloading').hide();
            }
        });
    } else {
        $('#addSalonservicesbtn').show();
        $('#addSalonservicesbtnloading').hide();
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: errorMsg,
            showConfirmButton: false,
            timer: 3000
        });
    }
    return false;
}


