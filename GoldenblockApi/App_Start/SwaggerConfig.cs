using System.Web.Http;
using WebActivatorEx;
using GoldenblockApi;
using Swashbuckle.Application;
using Goldenblock.Common;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace GoldenblockApi
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        
                        c.RootUrl(req => Configurations.PortalURL);                       
                      
                        c.SingleApiVersion("v1", "GoldenblockApi");
                        
                    })
                .EnableSwaggerUi(c =>
                    {
                       
                    });
        }
    }
}
