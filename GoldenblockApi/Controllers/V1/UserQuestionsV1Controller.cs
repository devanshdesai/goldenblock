﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserQuestionsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserQuestionsServices abstractUserQuestionsServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public UserQuestionsV1Controller(AbstractUserQuestionsServices abstractUserQuestionsServices)
        {
            this.abstractUserQuestionsServices = abstractUserQuestionsServices;
        }
        #endregion

        ///<Summary>
        /// UserQuestions_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserQuestions_Upsert")]
        public async Task<IHttpActionResult> UserQuestions_Upsert(UserQuestions userQuestions)
        {
            var quote = abstractUserQuestionsServices.UserQuestions_Upsert(userQuestions);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///UserQuestions_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserQuestions_All")]
        public async Task<IHttpActionResult> UserQuestions_All(PageParam pageParam, string search = "", long UserId = 0)
        {
            var quote = abstractUserQuestionsServices.UserQuestions_All(pageParam, search, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///UserQuestions_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserQuestions_ById")]
        public async Task<IHttpActionResult> UserQuestions_ById(long Id)
        {
            var quote = abstractUserQuestionsServices.UserQuestions_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}