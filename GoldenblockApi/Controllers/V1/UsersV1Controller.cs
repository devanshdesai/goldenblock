﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUsersServices abstractUsersServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public UsersV1Controller(AbstractUsersServices abstractUsersServices)
        {
            this.abstractUsersServices = abstractUsersServices;
        }
        #endregion


        ///<Summary>
        /// Users_All Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_All")]
        public async Task<IHttpActionResult> Users_All(PageParam pageParam, string search = "", long LookupUserTypeId = 0, long LookupCountryId = 0, long LookupStateId = 0, long LookupCityId = 0, long LookupStatusId = 0)
        {
            var quote = abstractUsersServices.Users_All(pageParam, search, LookupUserTypeId, LookupCountryId, LookupStateId, LookupCityId, LookupStatusId); ;
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        /// Users_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Upsert")]
        public async Task<IHttpActionResult> Users_Upsert(Users users)
        {
            var quote = abstractUsersServices.Users_Upsert(users);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///Users_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_ById")]
        public async Task<IHttpActionResult> Users_ById(long Id)
        {
            var quote = abstractUsersServices.Users_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///Users_Delete API
        ///</Summary>
        ///<param></param>       
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Delete")]
        public async Task<IHttpActionResult> Users_Delete(long Id, long DeletedBy)
        {
            var quote = abstractUsersServices.Users_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///Users_Login API
        ///</Summary>
        ///<param></param>  
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Login")]
        public async Task<IHttpActionResult> Users_Login(string Email, string Password)
        {
            var quote = abstractUsersServices.Users_Login(Email, Password);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///Users_Logout API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Logout")]
        public async Task<IHttpActionResult> Users_Logout(long Id)
        {
            var quote = abstractUsersServices.Users_ById(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///Users_IsAgreeTermsAndConditions API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_IsAgreeTermsAndConditions")]
        public async Task<IHttpActionResult> Users_IsAgreeTermsAndConditions(long Id)
        {
            var quote = abstractUsersServices.Users_IsAgreeTermsAndConditions(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///Users_IsEmailVerified API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_IsEmailVerified")]
        public async Task<IHttpActionResult> Users_IsEmailVerified(long Id)
        {
            var quote = abstractUsersServices.Users_IsEmailVerified(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///Users_ChangePassword API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_ChangePassword")]
        public async Task<IHttpActionResult> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            var quote = abstractUsersServices.Users_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///Users_UpdateLookupStatusId API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_UpdateLookupStatusId")]
        public async Task<IHttpActionResult> Users_UpdateLookupStatusId(long Id, long LookupStatusId = 0, long UpdatedBy = 0)
        {
            var quote = abstractUsersServices.Users_UpdateLookupStatusId(Id, LookupStatusId, UpdatedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        ///<Summary>
        ///SendResetPasswordLinkBy_UserEmail API
        ///</Summary>
        ///<param></param>  
        [System.Web.Http.HttpPost]
        [InheritedRoute("SendResetPasswordLinkBy_UserEmail")]
        public async Task<IHttpActionResult> SendResetPasswordLinkBy_UserEmail(string Email)
        {
            var quote = abstractUsersServices.SendResetPasswordLinkBy_UserEmail(Email);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        ///<Summary>
        ///ResetPasswordBy_UserEmail API
        ///</Summary>
        ///<param></param>  
        [System.Web.Http.HttpPost]
        [InheritedRoute("ResetPasswordBy_UserEmail")]
        public async Task<IHttpActionResult> ResetPasswordBy_UserEmail(string Email, string ResetCode, string NewPassword, string ConfirmPassword)
        {
            var quote = abstractUsersServices.ResetPasswordBy_UserEmail(Email, ResetCode, NewPassword, ConfirmPassword);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}