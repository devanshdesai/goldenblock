﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupStateV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupStateServices abstractLookupStateServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupStateV1Controller(AbstractLookupStateServices abstractLookupStateServices)
        {
            this.abstractLookupStateServices = abstractLookupStateServices;
        }
        #endregion


        ///<Summary>
        /// LookupState_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupState_Upsert")]
        public async Task<IHttpActionResult> LookupState_Upsert(LookupState lookupState)
        {
            var quote = abstractLookupStateServices.LookupState_Upsert(lookupState);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupState_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupState_All")]
        public async Task<IHttpActionResult> LookupState_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupStateServices.LookupState_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///LookupState_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupState_ById")]
        public async Task<IHttpActionResult> LookupState_ById(long Id)
        {
            var quote = abstractLookupStateServices.LookupState_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}