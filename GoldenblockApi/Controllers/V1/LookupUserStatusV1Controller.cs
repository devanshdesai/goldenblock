﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;


namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupUserStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupUserStatusServices abstractLookupUserStatusServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupUserStatusV1Controller(AbstractLookupUserStatusServices abstractLookupUserStatusServices)
        {
            this.abstractLookupUserStatusServices = abstractLookupUserStatusServices;
        }
        #endregion


        ///<Summary>
        /// LookupUserStatus_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupUserStatus_Upsert")]
        public async Task<IHttpActionResult> LookupUserStatus_Upsert(LookupUserStatus lookupUserStatus)
        {
            var quote = abstractLookupUserStatusServices.LookupUserStatus_Upsert(lookupUserStatus);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupUserStatus_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupUserStatus_All")]
        public async Task<IHttpActionResult> LookupUserStatus_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupUserStatusServices.LookupUserStatus_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}