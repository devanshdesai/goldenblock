﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;


namespace GoldenblockApi.Controllers.V1
{

    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PropertyV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPropertyServices abstractPropertyServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public PropertyV1Controller(AbstractPropertyServices abstractPropertyServices)
        {
            this.abstractPropertyServices = abstractPropertyServices;
        }
        #endregion


        ///<Summary>
        /// Property_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Property_Upsert")]
        public async Task<IHttpActionResult> Property_Upsert(Property property)
        {
            var quote = abstractPropertyServices.Property_Upsert(property);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///Property_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Property_All")]
        public async Task<IHttpActionResult> Property_All(PageParam pageParam, string search = "", long UserId = 0, long LookupConstructionPeriodId = 0, long LookupReferenceDateId = 0, long LookupStatusId = 0)
        {
            var quote = abstractPropertyServices.Property_All(pageParam, search, UserId, LookupConstructionPeriodId, LookupReferenceDateId, LookupStatusId);

            CustomPropertyLst obj = new CustomPropertyLst();
            obj.RecruitmentProperties = new List<AbstractProperty>();
            obj.RecruitmentPlannedProperties = new List<AbstractProperty>();
            obj.ClosedGoods = new List<AbstractProperty>();

            if (quote.Values.Count > 0)
            {
                foreach(var item in quote.Values)
                {
                    if(item.PropertyInvestmentStatusId == 1)
                    {
                        obj.RecruitmentProperties.Add(item);
                    }
                    else if (item.PropertyInvestmentStatusId == 2)
                    {
                        obj.RecruitmentProperties.Add(item);
                    }
                    else if (item.PropertyInvestmentStatusId == 3)
                    {
                        obj.RecruitmentProperties.Add(item);
                    }
                }
            }

            return this.Content((HttpStatusCode)200, obj);
        }

        ///<Summary>
        ///Property_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Property_ById")]
        public async Task<IHttpActionResult> Property_ById(long Id)
        {
            var quote = abstractPropertyServices.Property_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);

        }

        ///<Summary>
        ///Property_ByUserId API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Property_ByUserId")]
        public async Task<IHttpActionResult> Property_ByUserId(PageParam pageParam, long UserId = 0)
        {
            var quote = abstractPropertyServices.Property_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);

        }

        ///<Summary>
        ///Property_Delete API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Property_Delete")]
        public async Task<IHttpActionResult> Property_Delete(long Id, long DeletedBy)
        {
            var quote = abstractPropertyServices.Property_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///Property_UpdateLookupStatusId API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("Property_UpdateLookupStatusId")]
        public async Task<IHttpActionResult> Property_UpdateLookupStatusId(long Id = 0, long LookupStatusId = 0, long UpdatedBy = 0)
        {
            var quote = abstractPropertyServices.Property_UpdateLookupStatusId(Id, LookupStatusId, UpdatedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}