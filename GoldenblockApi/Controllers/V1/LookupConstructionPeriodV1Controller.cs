﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupConstructionPeriodV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupConstructionPeriodServices abstractLookupConstructionPeriodServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupConstructionPeriodV1Controller(AbstractLookupConstructionPeriodServices abstractLookupConstructionPeriodServices)
        {
            this.abstractLookupConstructionPeriodServices = abstractLookupConstructionPeriodServices;
        }
        #endregion

        ///<Summary>
        /// LookupConstructionPeriod_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupConstructionPeriod_Upsert")]
        public async Task<IHttpActionResult> LookupConstructionPeriod_Upsert(LookupConstructionPeriod lookupConstructionPeriod)
        {
            var quote = abstractLookupConstructionPeriodServices.LookupConstructionPeriod_Upsert(lookupConstructionPeriod);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupConstructionPeriod_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupConstructionPeriod_All")]
        public async Task<IHttpActionResult> LookupConstructionPeriod_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupConstructionPeriodServices.LookupConstructionPeriod_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///LookupConstructionPeriod_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupConstructionPeriod_ById")]
        public async Task<IHttpActionResult> LookupConstructionPeriod_ById(long Id)
        {
            var quote = abstractLookupConstructionPeriodServices.LookupConstructionPeriod_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}