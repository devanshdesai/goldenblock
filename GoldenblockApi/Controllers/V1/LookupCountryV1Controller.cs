﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupCountryV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupCountryServices abstractLookupCountryServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupCountryV1Controller(AbstractLookupCountryServices abstractLookupCountryServices)
        {
            this.abstractLookupCountryServices = abstractLookupCountryServices;
        }
        #endregion

        ///<Summary>
        /// LookupCountry_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupCountry_Upsert")]
        public async Task<IHttpActionResult> LookupCountry_Upsert(LookupCountry lookupCountry)
        {
            var quote = abstractLookupCountryServices.LookupCountry_Upsert(lookupCountry);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupCountry_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupCountry_All")]
        public async Task<IHttpActionResult> LookupCountry_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupCountryServices.LookupCountry_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///LookupCountry_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupCountry_ById")]
        public async Task<IHttpActionResult> LookupCountry_ById(long Id)
        {
            var quote = abstractLookupCountryServices.LookupCountry_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}