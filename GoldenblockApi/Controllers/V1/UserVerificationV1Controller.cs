﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserVerificationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserVerificationCodeServices abstractUserVerificationCodeServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public UserVerificationV1Controller(AbstractUserVerificationCodeServices abstractUserVerificationCodeServices)
        {
            this.abstractUserVerificationCodeServices = abstractUserVerificationCodeServices;
        }
        #endregion


        ///<Summary>
        /// UserVerificationCode_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserVerificationCode_Upsert")]
        public async Task<IHttpActionResult> UserVerificationCode_Upsert(UserVerificationCode userVerificationCode)
        {
            var quote = abstractUserVerificationCodeServices.UserVerificationCode_Upsert(userVerificationCode);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        /// UserVerificationCode_SendVerificationCode Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserVerificationCode_SendVerificationCode")]
        public async Task<IHttpActionResult> UserVerificationCode_SendVerificationCode(UserVerificationCode userVerificationCode)
        {
            var quote = abstractUserVerificationCodeServices.UserVerificationCode_SendVerificationCode(userVerificationCode);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///UserVerificationCode_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserVerificationCode_All")]
        public async Task<IHttpActionResult> UserVerificationCode_All(PageParam pageParam, string search = "",long UserId = 0)
        {
            var quote = abstractUserVerificationCodeServices.UserVerificationCode_All(pageParam, search, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///UserVerificationCode_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserVerificationCode_ById")]
        public async Task<IHttpActionResult> UserVerificationCode_ById(long Id)
        {
            var quote = abstractUserVerificationCodeServices.UserVerificationCode_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
       
    }
}