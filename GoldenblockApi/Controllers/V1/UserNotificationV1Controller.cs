﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserNotificationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserNotificationServices abstractUserNotificationServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public UserNotificationV1Controller(AbstractUserNotificationServices abstractUserNotificationServices)
        {
            this.abstractUserNotificationServices = abstractUserNotificationServices;
        }
        #endregion


        ///<Summary>
        /// UserNotification_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotification_Upsert")]
        public async Task<IHttpActionResult> UserNotification_Upsert(UserNotification userNotification)
        {
            var quote = abstractUserNotificationServices.UserNotification_Upsert(userNotification);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///UserNotification_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotification_All")]
        public async Task<IHttpActionResult> UserNotification_All(PageParam pageParam, string search = "", long UserId = 0, long LookupNotificationTypeId = 0)
        {
            var quote = abstractUserNotificationServices.UserNotification_All(pageParam, search, UserId, LookupNotificationTypeId);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///UserNotification_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotification_ById")]
        public async Task<IHttpActionResult> UserNotification_ById(long Id)
        {
            var quote = abstractUserNotificationServices.UserNotification_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);

        }

        ///<Summary>
        ///UserNotification_ByUserId API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotification_ByUserId")]
        public async Task<IHttpActionResult> UserNotification_ByUserId(PageParam pageParam, long UserId = 0)
        {
            var quote = abstractUserNotificationServices.UserNotification_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);

        }
    }
}