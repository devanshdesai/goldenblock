﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PropertyFrequentlyAskedQuestionsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPropertyFrequentlyAskedQuestionsServices abstractPropertyFrequentlyAskedQuestions;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public PropertyFrequentlyAskedQuestionsV1Controller(AbstractPropertyFrequentlyAskedQuestionsServices abstractPropertyFrequentlyAskedQuestions)
        {
            this.abstractPropertyFrequentlyAskedQuestions = abstractPropertyFrequentlyAskedQuestions;
        }
        #endregion


        ///<Summary>
        /// PropertyFrequentlyAskedQuestions_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyFrequentlyAskedQuestions_Upsert")]
        public async Task<IHttpActionResult> PropertyFrequentlyAskedQuestions_Upsert(PropertyFrequentlyAskedQuestions propertyFrequentlyAskedQuestions)
        {
            var quote = abstractPropertyFrequentlyAskedQuestions.PropertyFrequentlyAskedQuestions_Upsert(propertyFrequentlyAskedQuestions);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///PropertyFrequentlyAskedQuestions_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyFrequentlyAskedQuestions_All")]
        public async Task<IHttpActionResult> PropertyFrequentlyAskedQuestions_All(PageParam pageParam, string search = "", long PropertyId = 0)
        {
            var quote = abstractPropertyFrequentlyAskedQuestions.PropertyFrequentlyAskedQuestions_All(pageParam, search, PropertyId);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///PropertyFrequentlyAskedQuestions_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyFrequentlyAskedQuestions_ById")]
        public async Task<IHttpActionResult> PropertyFrequentlyAskedQuestions_ById(long Id)
        {
            var quote = abstractPropertyFrequentlyAskedQuestions.PropertyFrequentlyAskedQuestions_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);

        }

        ///<Summary>
        ///PropertyFrequentlyAskedQuestions_PropertyId API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyFrequentlyAskedQuestions_PropertyId")]
        public async Task<IHttpActionResult> PropertyFrequentlyAskedQuestions_PropertyId(PageParam pageParam, long PropertyId = 0)
        {
            var quote = abstractPropertyFrequentlyAskedQuestions.PropertyFrequentlyAskedQuestions_PropertyId(pageParam, PropertyId);
            return this.Content((HttpStatusCode)200, quote);

        }

        ///<Summary>
        ///PropertyFrequentlyAskedQuestions_Delete API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyFrequentlyAskedQuestions_Delete")]
        public async Task<IHttpActionResult> PropertyFrequentlyAskedQuestions_Delete(long Id, long DeletedBy)
        {
            var quote = abstractPropertyFrequentlyAskedQuestions.PropertyFrequentlyAskedQuestions_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}