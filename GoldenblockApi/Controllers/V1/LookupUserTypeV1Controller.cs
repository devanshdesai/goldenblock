﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupUserTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupUserTypeServices abstractLookupUserTypeServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupUserTypeV1Controller(AbstractLookupUserTypeServices abstractLookupUserTypeServices)
        {
            this.abstractLookupUserTypeServices = abstractLookupUserTypeServices;
        }
        #endregion


        ///<Summary>
        /// LookupUserType_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupUserType_Upsert")]
        public async Task<IHttpActionResult> LookupUserType_Upsert(LookupUserType lookupUserType)
        {
            var quote = abstractLookupUserTypeServices.LookupUserType_Upsert(lookupUserType);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupUserType_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupUserType_All")]
        public async Task<IHttpActionResult> LookupUserType_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupUserTypeServices.LookupUserType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}