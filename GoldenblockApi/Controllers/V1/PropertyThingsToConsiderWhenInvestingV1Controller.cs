﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PropertyThingsToConsiderWhenInvestingV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPropertyThingsToConsiderWhenInvestingServices abstractPropertyThingsToConsiderInvestingServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public PropertyThingsToConsiderWhenInvestingV1Controller(AbstractPropertyThingsToConsiderWhenInvestingServices abstractPropertyThingsToConsiderInvestingServices)
        {
            this.abstractPropertyThingsToConsiderInvestingServices = abstractPropertyThingsToConsiderInvestingServices;
        }
        #endregion


        ///<Summary>
        /// PropertyThingsToConsiderWhenInvesting_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyThingsToConsiderWhenInvesting_Upsert")]
        public async Task<IHttpActionResult> PropertyThingsToConsiderWhenInvesting_Upsert(PropertyThingsToConsiderWhenInvesting propertyThingsToConsider)
        {
            var quote = abstractPropertyThingsToConsiderInvestingServices.PropertyThingsToConsiderWhenInvesting_Upsert(propertyThingsToConsider);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///PropertyThingsToConsiderWhenInvesting_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyThingsToConsiderWhenInvesting_All")]
        public async Task<IHttpActionResult> PropertyThingsToConsiderWhenInvesting_All(PageParam pageParam, string search = "", long PropertyId = 0)
        {
            var quote = abstractPropertyThingsToConsiderInvestingServices.PropertyThingsToConsiderWhenInvesting_All(pageParam, search, PropertyId);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///PropertyThingsToConsiderWhenInvesting_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyThingsToConsiderWhenInvesting_ById")]
        public async Task<IHttpActionResult> PropertyThingsToConsiderWhenInvesting_ById(long Id)
        {
            var quote = abstractPropertyThingsToConsiderInvestingServices.PropertyThingsToConsiderWhenInvesting_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);

        }

        ///<Summary>
        ///PropertyThingsToConsiderWhenInvesting_PropertyId API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyThingsToConsiderWhenInvesting_PropertyId")]
        public async Task<IHttpActionResult> PropertyThingsToConsiderWhenInvesting_PropertyId(PageParam pageParam, long PropertyId = 0)
        {
            var quote = abstractPropertyThingsToConsiderInvestingServices.PropertyThingsToConsiderWhenInvesting_PropertyId(pageParam, PropertyId);
            return this.Content((HttpStatusCode)200, quote);

        }

        ///<Summary>
        ///PropertyThingsToConsiderWhenInvesting_Delete API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("PropertyThingsToConsiderWhenInvesting_Delete")]
        public async Task<IHttpActionResult> PropertyThingsToConsiderWhenInvesting_Delete(long Id, long DeletedBy)
        {
            var quote = abstractPropertyThingsToConsiderInvestingServices.PropertyThingsToConsiderWhenInvesting_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}