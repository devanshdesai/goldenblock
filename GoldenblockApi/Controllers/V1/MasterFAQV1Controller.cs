﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterFAQV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterFAQServices abstractMasterFAQServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public MasterFAQV1Controller(AbstractMasterFAQServices abstractMasterFAQServices)
        {
            this.abstractMasterFAQServices = abstractMasterFAQServices;
        }
        #endregion

        ///<Summary>
        /// MasterFAQ_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterFAQ_Upsert")]
        public async Task<IHttpActionResult> MasterFAQ_Upsert(MasterFAQ masterFAQ)
        {
            var quote = abstractMasterFAQServices.MasterFAQ_Upsert(masterFAQ);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///MasterFAQ_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterFAQ_All")]
        public async Task<IHttpActionResult> MasterFAQ_All(PageParam pageParam, string search = "")
        {
            var quote = abstractMasterFAQServices.MasterFAQ_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///MasterFAQ_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterFAQ_ById")]
        public async Task<IHttpActionResult> MasterFAQ_ById(long Id)
        {
            var quote = abstractMasterFAQServices.MasterFAQ_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}