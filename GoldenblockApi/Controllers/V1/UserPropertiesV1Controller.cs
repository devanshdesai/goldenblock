﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{

    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserPropertiesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserPropertiesServices abstractUserPropertiesServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public UserPropertiesV1Controller(AbstractUserPropertiesServices abstractUserPropertiesServices)
        {
            this.abstractUserPropertiesServices = abstractUserPropertiesServices;
        }
        #endregion


        ///<Summary>
        /// UserProperties_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserProperties_Upsert")]
        public async Task<IHttpActionResult> UserProperties_Upsert(UserProperties userProperties)
        {
            var quote = abstractUserPropertiesServices.UserProperties_Upsert(userProperties);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///UserProperties_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserProperties_All")]
        public async Task<IHttpActionResult> UserProperties_All(PageParam pageParam, string search = "", long PropertyId = 0 ,long UserId = 0)
        {
            var quote = abstractUserPropertiesServices.UserProperties_All(pageParam, search, PropertyId, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///UserProperties_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserProperties_ById")]
        public async Task<IHttpActionResult> UserProperties_ById(long Id)
        {
            var quote = abstractUserPropertiesServices.UserProperties_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);

        }

        ///<Summary>
        ///UserProperties_ByPropertyId API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserProperties_ByPropertyId")]
        public async Task<IHttpActionResult> UserProperties_ByPropertyId(PageParam pageParam, long PropertyId = 0)
        {
            var quote = abstractUserPropertiesServices.UserProperties_ByPropertyId(pageParam, PropertyId);
            return this.Content((HttpStatusCode)200, quote);

        }

        ///<Summary>
        ///UserProperties_ByUserId API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserProperties_ByUserId")]
        public async Task<IHttpActionResult> UserProperties_ByUserId(PageParam pageParam, long UserId)
        {
            var quote = abstractUserPropertiesServices.UserProperties_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);

        }
    }
}