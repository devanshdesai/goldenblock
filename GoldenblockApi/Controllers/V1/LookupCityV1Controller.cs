﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupCityV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupCityServices abstractLookupCityServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupCityV1Controller(AbstractLookupCityServices abstractLookupCityServices)
        {
            this.abstractLookupCityServices = abstractLookupCityServices;
        }
        #endregion


        ///<Summary>
        /// LookupCity_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupCity_Upsert")]       
        public async Task<IHttpActionResult> LookupCity_Upsert(LookupCity lookupCity)
        {
            var quote = abstractLookupCityServices.LookupCity_Upsert(lookupCity);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupCity_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupCity_All")]
        public async Task<IHttpActionResult> LookupCity_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupCityServices.LookupCity_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///LookupCity_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupCity_ById")]
        public async Task<IHttpActionResult> LookupCity_ById(long Id)
        {
            var quote = abstractLookupCityServices.LookupCity_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}