﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{

    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupReferenceDateV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupReferenceDateServices abstractLookupReferenceDateServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupReferenceDateV1Controller(AbstractLookupReferenceDateServices abstractLookupReferenceDateServices)
        {
            this.abstractLookupReferenceDateServices = abstractLookupReferenceDateServices;
        }
        #endregion


        ///<Summary>
        /// LookupReferenceDate_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupReferenceDate_Upsert")]
        public async Task<IHttpActionResult> LookupReferenceDate_Upsert(LookupReferenceDate lookupReferenceDate)
        {
            var quote = abstractLookupReferenceDateServices.LookupReferenceDate_Upsert(lookupReferenceDate);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupReferenceDate_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupReferenceDate_All")]
        public async Task<IHttpActionResult> LookupReferenceDate_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupReferenceDateServices.LookupReferenceDate_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        ///<Summary>
        ///LookupReferenceDate_ById API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupReferenceDate_ById")]
        public async Task<IHttpActionResult> LookupReferenceDate_ById(long Id)
        {
            var quote = abstractLookupReferenceDateServices.LookupReferenceDate_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}