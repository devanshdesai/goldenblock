﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupPropertyStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupPropertyStatusServices abstractLookupPropertyStatusServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupPropertyStatusV1Controller(AbstractLookupPropertyStatusServices abstractLookupPropertyStatusServices)
        {
            this.abstractLookupPropertyStatusServices = abstractLookupPropertyStatusServices;
        }
        #endregion


        ///<Summary>
        /// LookupPropertyStatus_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupPropertyStatus_Upsert")]
        public async Task<IHttpActionResult> LookupPropertyStatus_Upsert(LookupPropertyStatus lookupPropertyStatus)
        {
            var quote = abstractLookupPropertyStatusServices.LookupPropertyStatus_Upsert(lookupPropertyStatus);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupPropertyStatus_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupPropertyStatus_All")]
        public async Task<IHttpActionResult> LookupPropertyStatus_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupPropertyStatusServices.LookupPropertyStatus_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}