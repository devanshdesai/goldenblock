﻿using Goldenblock.APICommon;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.V1;
using Goldenblock.Services.Contract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoldenblockApi.Controllers.V1
{
    ///<Summary>
    /// Controllers
    ///</Summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupNotificationTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupNotificationTypeServices abstractLookupNotificationTypeServices;
        #endregion

        #region Cnstr
        ///<Summary>
        /// constructor
        ///</Summary>
        public LookupNotificationTypeV1Controller(AbstractLookupNotificationTypeServices abstractLookupNotificationTypeServices)
        {
            this.abstractLookupNotificationTypeServices = abstractLookupNotificationTypeServices;
        }
        #endregion


        ///<Summary>
        /// LookupNotificationType_Upsert Api   
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupNotificationType_Upsert")]
        public async Task<IHttpActionResult> LookupNotificationType_Upsert(LookupNotificationType lookupNotificationType)
        {
            var quote = abstractLookupNotificationTypeServices.LookupNotificationType_Upsert(lookupNotificationType);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ///<Summary>
        ///LookupNotificationType_All API
        ///</Summary>
        ///<param></param>
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupNotificationType_All")]
        public async Task<IHttpActionResult> LookupNotificationType_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupNotificationTypeServices.LookupNotificationType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}