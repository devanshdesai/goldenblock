﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";

}

var DeviceTokenNumber = getCookie("DeviceToken&Type");
var VendorId = getCookie("UserId");

//fillter function
function seachProduct() {
    $('#searchProductbtn').hide();
    $('#searchProductbtnloading').show();
    productList.init();
}

//fllter reset function
function resetProduct() {
    $('#resetProductbtn').hide();
    $('#resetProductbtnloading').show();
    $('#productName').val('');
    $('#productType').selectpicker('val', null);
    $('#productBrand').selectpicker('val', null);
    $('#productStatus').selectpicker('val', null);
    productList.init();
}

var productList = function () {

    $('#productListloader').show();
    debugger;
    let initProductlist = function () {
        var productName = $('#productName').val();
        var productType = $('#productType').val() == "" || $('#productType').val() == null ? 0 : $('#productType').val();
        var productBrand = $('#productBrand').val() == "" || $('#productBrand').val() == null ? 0 : $('#productBrand').val();
        var productStatus = $('#productStatus').val() == "" || $('#productStatus').val() == null ? 0 : $('#productStatus').val();

        let ProductList = new Object();
        ProductList.IsPageProvided = true;

        $.ajax({
            processing: true,
            serverSide: true,
            type: 'POST',
            url: APIEndPoint + `/api/product/Product_All?search=null&ProductName=${productName}&ProductTypeId=${productType}&ProductBrandId=${productBrand}&LookUpStatusId=${productStatus}&VendorId=${atob(VendorId)}&ProductBrandIds=&ProductCategoryIds=&MinPrice=&MaxPrice=`,
            headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
            dataType: 'json',
            data: JSON.stringify(ProductList),
            crossDomain: true,
            success: function (Values) {
                console.log(Values);
                var i = 1;
                debugger;
                $('#productList').DataTable({
                    "order": [[0, "desc"]],
                    data: Values.Values,
                    
                    columns: [
                        {
                            "title": "Id", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${i++}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "1%"
                        },
                        {
                            "title": "Product Name", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["ProductName"] == "" || row["ProductName"] == null ? '-' : row["ProductName"]}
                                                <spna>${row["OfferPrice"] > 0 ? '<div class="badge bg-soft-success text-success border p-1">Offer</span>' : ''}</span>`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "Product Type", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["ProductTypeName"] == "" || row["ProductTypeName"] == null ? '-' : row["ProductTypeName"]}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "Brand", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["ProductBrandName"] == "" || row["ProductBrandName"] == null ? '-' : row["ProductBrandName"]}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "Weight", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["Weight"] == "" || row["Weight"] == null ? '-' : row["Weight"]} 
                                            ${row["ProductWeightTypeName"] == "" || row["ProductWeightTypeName"] == null ? '' : row["ProductWeightTypeName"]}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "In Stock", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `<b>${row["ProductQty"] == "" || row["ProductQty"] == null ? '-' : row["ProductQty"]}</b>`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "Low QTY. Alert", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `${row["LowQtyAlert"] == "" || row["LowQtyAlert"] == null ? '-' : row["LowQtyAlert"]}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "3%"
                        },
                        {
                            "title": "Status", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData += `${row["LookUpStatusId"] == 13 ? `<div class="badge bg-soft-success text-success border p-2" onclick="changeStatusmodal(${row["Id"]});">${row["LookUpStatusName"]}</div>` : ''}`;
                                htmlData += `${row["LookUpStatusId"] == 14 ? `<div class="badge bg-soft-danger text-danger border p-2" onclick="changeStatusmodal(${row["Id"]});">${row["LookUpStatusName"]}</div>` : ''}`;
                                htmlData += `${row["LookUpStatusId"] == 15 ? `<div class="badge bg-soft-secondary text-secondary border p-2" onclick="changeStatusmodal(${row["Id"]});">${row["LookUpStatusName"]}</div>` : ''}`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "2%"
                        },
                        {
                            "title": "Action", "data": "",
                            "render": function (data, type, row) {
                                let htmlData = "";
                                htmlData = `<div class="dropdown">
                                                <button class="btn btn-icon" type="button" data-toggle="dropdown">
                                                    <i class="bb-more-horizontal fs-22"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="javascript:void(0)" onclick="updateQty(${row["Id"]});"><i class="bb-repeat text-gray-600 fs-16 mr-3"></i>Update QTY</a>
                                                    <a class="dropdown-item" href="/VendorProduct/ProductDetails?productId=${btoa(row["Id"])}"><i class="bb-edit-3 text-gray-600 fs-16 mr-3"></i>Edit</a>
                                                    <hr>
                                                    <a class="dropdown-item text-danger" href="javascript:void(0)" onclick="productDelete(${row["Id"]})"><i class="bb-trash-2 text-danger fs-16 mr-3"></i>Delete</a>
                                                </div>
                                            </div>`;
                                return htmlData;
                            }
                            , "orderable": false, "width": "2%"
                        },
                    ],
                    buttons: [
                        {
                            className: 'btn btn-primary float-left mb-3',
                            text: 'Add New Product',
                            action: function (e, dt, node, config) {
                                window.location = '/VendorProduct/ProductDetails';
                            }
                        },
                        {
                            extend: 'pdf',
                            className: 'btn btn-light border font-weight-medium float-right mb-3',
                            text: '<i class="bb-printer fs-16 mr-2"></i>Print',
                        },
                        {
                            extend: 'excel',
                            className: 'btn btn-light border font-weight-medium float-right mb-3 mr-2',
                            text: '<i class="bb-download fs-16 mr-2"></i> Export to Excel',
                        },
                    ],
                    responsive: true,
                    "lengthMenu": [
                        [5, 15, 20, 40],
                        [5, 15, 20, 40] // change per page values here
                    ],
                    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                });
               
                $('#searchProductbtn').show();
                $('#searchProductbtnloading').hide();

                $('#resetProductbtn').show();
                $('#resetProductbtnloading').hide();

                $('#productListloader').hide();

            }, error: function (error) {
                $('#searchProductbtn').show();
                $('#searchProductbtnloading').hide();

                $('#resetProductbtn').show();
                $('#resetProductbtnloading').hide();

                $('#productListloader').hide();

            }
        });
    }
    return {
        //main function to initiate the module
        init: function () {
            if ($.fn.DataTable.isDataTable("#productList")) {
                $('#productList').dataTable().fnDestroy();
                $('#manageProductdiv').html('<table id="productList" class="table table-card" style="width:100%; display:inherit;"</table >');
            }
            initProductlist();
        }
    };
}();