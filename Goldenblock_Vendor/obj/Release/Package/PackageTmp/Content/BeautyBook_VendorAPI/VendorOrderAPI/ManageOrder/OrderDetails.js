﻿//getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";

}

var DeviceTokenNumber = getCookie("DeviceToken&Type");

function vieworderDetails(orderId) {
    $('#ordersummaryetails').html(``);
    $('#orderitemdetails').html(``);
    $('#orderDetails').modal('show');


    $.ajax({
        type: 'POST',
        url: '' + APIEndPoint + `/api/orders/Orders_ById?Id=${orderId}`,
        headers: { 'Content-Type': 'application/json', "Authorization": '' + DeviceTokenNumber + '' },
        dataType: 'json',
        crossDomain: true,
        success: function (result) {
            debugger;
            $('#ordersummaryetails').append(`
                    <div class="col-lg-4 col-sm-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Order ID</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.OrderNo}</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Order Date</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.OrderDate}</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Total Payment</div>
                                <div>
                                    <span class="font-weight-medium">$${result.Item.TotalAmount}</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Payment</div>
                                <div>
                                    ${result.Item.Payment == 22 ? `<div class="badge badge-success border p-2">${result.Item.PaymentType}</div>` :``}
                                    ${result.Item.Payment == 23 ? `<div class="badge badge-primary border p-2">${result.Item.PaymentType}</div>` :``}
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Order Status</div>
                                <div>
                                    <div class="badge bg-soft-success text-success border p-2">${result.Item.StatusName}</div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Last Updated</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.UpdatedDateStr}</span>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Customer Name</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.SalonName}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <div class="text-gray-500 fs-13 mb-1">Shipment to</div>
                                <div>
                                    <span class="font-weight-medium">${result.Item.ShippingAddress}</span>
                                </div>
                            </div>
                `);
            // orderitemdetails data append

            if (result.Item.OrderProducts.length <= 0)
            {
                $('#orderitemdetails').html(`
                        <tr>
                            <td colspan="100" class="text-center" style="font-size:18px;font-weight:500;">No records found</td>
                        </tr>
                    `)
            } else {
                $("#orderitemdetails").html(``);
            }
            
            var orderdetailstable =
                `
                    <table class="table mb-0">
                    <thead>
                        <tr>
                            <th class="col-3">Product Name</th>
                            <th class="col-2">Rate</th>
                            <th class="col-1">QTY</th>
                            <th class="col-2 text-right">Amount</th>
                        </tr>
                    </thead>
                `;
            
                for (i = 0; i < result.Item.OrderProducts.length; i++) {
                    orderdetailstable += `
                        <tbody>
                            <tr>
                                <td class="col-3">${result.Item.OrderProducts[i].ProductName}</td>
                                <td class="col-2 text-center">${result.Item.OrderProducts[i].Rate}</td>
                                <td class="col-1 text-center">${result.Item.OrderProducts[i].Qty}</td>
                                <td class="col-2 text-right">${result.Item.OrderProducts[i].Amount}</td>
                            </tr>
                    `;
                }

            orderdetailstable += `
                            <tr>
                            <td colspan="3" class="text-right"><strong>Sub Total:</strong></td>
                            <td class="text-right">$${result.Item.OrderProducts[0].SubTotal}</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-right"><strong>Tax:</strong></td>
                            <td class="text-right">$${result.Item.OrderProducts[0].Tax}</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-right"><strong>Total:</strong></td>
                            <td class="text-right">$${result.Item.OrderProducts[0].Total}</td>
                        </tr>

`;
            orderdetailstable += `
        </tbody>
        </table>
        `;
            $("#orderitemdetails").append(orderdetailstable);
            // orderitemdetails data append
            if (result.Item.OrderStatusTracking.length <= 0) {
                $('#orderstatustracking').html(`
                        <tr>
                            <td colspan="100" class="text-center" style="font-size:18px;font-weight:500;">No records found</td>
                        </tr>
                    `)
            } else {
                $("#orderstatustracking").html(``);
            }
            for (i = 0; i < result.Item.OrderStatusTracking.length; i++) {
                $('#orderstatustracking').append(`
                   <div class="timeline-item done">
                    <h6 class="fs-14">${result.Item.OrderStatusTracking[i].LookUpStatus}</h6>
                    <div>
                        <p class="text-muted fs-14 mb-0 lh-1">${result.Item.OrderStatusTracking[i].Comment == null || result.Item.OrderStatusTracking[i].Comment == "" ? `-` : result.Item.OrderStatusTracking[i].Comment}</p>
                        <span class="text-muted fs-12">${result.Item.OrderStatusTracking[i].CreatedDateStr}</span>
                    </div>
                </div>
                `);
            }
        }
    });
    return false;
}