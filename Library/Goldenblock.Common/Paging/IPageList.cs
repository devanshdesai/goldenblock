﻿using System.Collections.Generic;

namespace Goldenblock.Common.Paging
{
    /// <summary>
    /// Paged list interface
    /// </summary>
    public interface IPagedList<T> : IList<T>
    {
        
    }
}
