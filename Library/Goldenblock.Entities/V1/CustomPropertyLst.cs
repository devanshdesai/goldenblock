﻿using Goldenblock.Entities.Contract;
using System.Collections.Generic;

namespace Goldenblock.Entities.V1
{
    public class CustomPropertyLst 
    {
        public List<AbstractProperty> RecruitmentProperties { get; set; }
        public List<AbstractProperty> RecruitmentPlannedProperties { get; set; }
        public List<AbstractProperty> ClosedGoods { get; set; }
    }
}
