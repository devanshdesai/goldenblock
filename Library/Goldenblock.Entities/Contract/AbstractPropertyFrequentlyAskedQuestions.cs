﻿using Goldenblock;
using Goldenblock.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Goldenblock.Entities.Contract
{
    public abstract class AbstractPropertyFrequentlyAskedQuestions
    {
        public long Id { get; set; }
        public long PropertyId { get; set; }
        public long UserId { get; set; }
        public string Title { get; set; }
        public decimal AnnualRateOfReturn { get; set; }
        public decimal TotalRecruitmentAmount { get; set; }
        public decimal InvestmentAmount { get; set; }
        public decimal InvestmentPeriod { get; set; }
        public string BusinessAddress { get; set; }

        public string BusinessName { get; set; }
        public string BusinessScale { get; set; }
        public decimal UseOfFunds { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

