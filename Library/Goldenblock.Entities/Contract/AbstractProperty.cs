﻿using Goldenblock;
using Goldenblock.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Goldenblock.Entities.Contract
{
    public abstract class AbstractProperty
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string UserFullName { get; set; }
        public string UserDOB { get; set; }
        public string UserEmail { get; set; }
        public string Title { get; set; }
        public long LookupConstructionPeriodId { get; set; }
        public string LookupConstructionPeriodName { get; set; }
        public decimal AnnualRateOfReturn { get; set; }
        public decimal TotalRecruitmentAmount { get; set; }
        public decimal InvestmentAmount { get; set; }
        public decimal InvestmentPeriod { get; set; }
        public decimal ExpectedProfit { get; set; }
        public string InvestorProtection { get; set; }
        public string BusinessAddress { get; set; }
        public string BusinessName { get; set; }
        public string BusinessScale { get; set; }
        public decimal UseOfFunds { get; set; }
        public decimal LTV { get; set; }
        public decimal RepaymentOrder { get; set; }
        public decimal FairRate { get; set; }
        public long LookupReferenceDateId { get; set; }
        public string LookupReferenceDateName { get; set; }
        public decimal SeniorLoan { get; set; }
        public decimal ErnestFundLoans { get; set; }
        public decimal OtherAmount { get; set; }
        public decimal ResidualAmountOfCollateralValue { get; set; }
        public string RepaymentScenario { get; set; }
        public string MajorRisk { get; set; }
        public string DetailedAnalysis { get; set; }
        public long LookupStatusId { get; set; }
        public string LookupStatusName { get; set; }
        public string ImageURL1 { get; set; }
        public string ImageURL2 { get; set; }
        public string ImageURL3 { get; set; }
        public string ImageURL4 { get; set; }
        public int PropertyInvestmentStatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }


        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

