﻿using Goldenblock;
using Goldenblock.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Goldenblock.Entities.Contract
{
    public abstract class AbstractUserProperties
    {
        public long Id { get; set; }
        public long PropertyId { get; set; }
        public long UserId { get; set; }
        public string Title { get; set; }
        public Decimal AnnualRateOfReturn { get; set; }
        public Decimal TotalRecruitmentAmount { get; set; }
        public Decimal InvestmentAmount { get; set; }
        public Decimal InvestmentPeriod { get; set; }
        public Decimal ExpectedProfit { get; set; }
        public string BusinessAddress { get; set; }
        public string BusinessName { get; set; }
        public string BusinessScale { get; set; }
        public Decimal UseOfFunds { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

