﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupNotificationTypeServices : AbstractLookupNotificationTypeServices
    {
        private AbstractLookupNotificationTypeDao abstractLookupNotificationTypeDao;

        public LookupNotificationTypeServices(AbstractLookupNotificationTypeDao abstractLookupNotificationTypeDao)
        {
            this.abstractLookupNotificationTypeDao = abstractLookupNotificationTypeDao;
        }

        public override SuccessResult<AbstractLookupNotificationType> LookupNotificationType_Upsert(AbstractLookupNotificationType abstractLookupNotificationType)
        {
            return this.abstractLookupNotificationTypeDao.LookupNotificationType_Upsert(abstractLookupNotificationType);
        }



        public override PagedList<AbstractLookupNotificationType> LookupNotificationType_All(PageParam pageParam, string search)
        {
            return this.abstractLookupNotificationTypeDao.LookupNotificationType_All(pageParam, search);

        }




    }
}
