﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class PropertyServices : AbstractPropertyServices
    {
        private AbstractPropertyDao abstractPropertyDao;

        public PropertyServices(AbstractPropertyDao abstractPropertyDao)
        {
            this.abstractPropertyDao = abstractPropertyDao;
        }

        public override PagedList<AbstractProperty> Property_All(PageParam pageParam, string search, long UserId, long LookupConstructionPeriodId, long LookupReferenceDateId, long LookupStatusId)
        {
            return this.abstractPropertyDao.Property_All(pageParam, search, UserId, LookupConstructionPeriodId, LookupReferenceDateId, LookupStatusId);
        }


        public override PagedList<AbstractProperty> Property_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractPropertyDao.Property_ByUserId(pageParam, UserId);
        }

        public override SuccessResult<AbstractProperty> Property_ById(long Id)
        {
            return this.abstractPropertyDao.Property_ById(Id);
        }

        
        public override SuccessResult<AbstractProperty> Property_Upsert(AbstractProperty abstractProperty)
        {
            return this.abstractPropertyDao.Property_Upsert(abstractProperty); ;
        }



        public override SuccessResult<AbstractProperty> Property_Delete(long Id, long DeletedBy)
        {
            return this.abstractPropertyDao.Property_Delete(Id, DeletedBy);
        }

        
        public override SuccessResult<AbstractProperty> Property_UpdateLookupStatusId(long Id, long LookupStatusId, long UpdatedBy)
        {
            return this.abstractPropertyDao.Property_UpdateLookupStatusId(Id, LookupStatusId, UpdatedBy);
        }
    }

}