﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupPropertyStatusServices : AbstractLookupPropertyStatusServices
    {
        private AbstractLookupPropertyStatusDao abstractLookupPropertyStatusDao;

        public LookupPropertyStatusServices(AbstractLookupPropertyStatusDao abstractLookupPropertyStatusDao)
        {
            this.abstractLookupPropertyStatusDao = abstractLookupPropertyStatusDao;
        }

        public override SuccessResult<AbstractLookupPropertyStatus> LookupPropertyStatus_Upsert(AbstractLookupPropertyStatus abstractLookupPropertyStatus)
        {
            return this.abstractLookupPropertyStatusDao.LookupPropertyStatus_Upsert(abstractLookupPropertyStatus);
        }



        public override PagedList<AbstractLookupPropertyStatus> LookupPropertyStatus_All(PageParam pageParam, string search)
        {
            return this.abstractLookupPropertyStatusDao.LookupPropertyStatus_All(pageParam, search);

        }




    }
}
