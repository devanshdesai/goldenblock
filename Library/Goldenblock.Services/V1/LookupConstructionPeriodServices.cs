﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupConstructionPeriodServices : AbstractLookupConstructionPeriodServices
    {
        private AbstractLookupConstructionPeriodDao abstractLookupConstructionPeriodDao;

        public LookupConstructionPeriodServices(AbstractLookupConstructionPeriodDao abstractLookupConstructionPeriodDao)
        {
            this.abstractLookupConstructionPeriodDao = abstractLookupConstructionPeriodDao;
        }

        public override SuccessResult<AbstractLookupConstructionPeriod> LookupConstructionPeriod_Upsert(AbstractLookupConstructionPeriod abstractLookupConstructionPeriod)
        {
            return this.abstractLookupConstructionPeriodDao.LookupConstructionPeriod_Upsert(abstractLookupConstructionPeriod);
        }

        public override SuccessResult<AbstractLookupConstructionPeriod> LookupConstructionPeriod_ById(long Id)
        {
            return this.abstractLookupConstructionPeriodDao.LookupConstructionPeriod_ById(Id);
        }

        public override PagedList<AbstractLookupConstructionPeriod> LookupConstructionPeriod_All(PageParam pageParam, string search)
        {
            return this.abstractLookupConstructionPeriodDao.LookupConstructionPeriod_All(pageParam, search);

        }


    }
}
