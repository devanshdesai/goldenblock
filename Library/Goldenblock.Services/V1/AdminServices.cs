﻿using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class AdminServices : AbstractAdminServices
    {
        private AbstractAdminDao abstractAdminDao;

        public AdminServices(AbstractAdminDao abstractAdminDao)
        {
            this.abstractAdminDao = abstractAdminDao;
        }

        public override SuccessResult<AbstractAdmins> Admin_Login(string Email, string Password)
        {
            return this.abstractAdminDao.Admin_Login(Email, Password);
        }
    }
}
