﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupUserTypeServices : AbstractLookupUserTypeServices
    {
        private AbstractLookupUserTypeDao abstractLookupUserTypeDao;

        public LookupUserTypeServices(AbstractLookupUserTypeDao abstractLookupUserTypeDao)
        {
            this.abstractLookupUserTypeDao = abstractLookupUserTypeDao;
        }

        public override SuccessResult<AbstractLookupUserType> LookupUserType_Upsert(AbstractLookupUserType abstractLookupUserType)
        {
            return this.abstractLookupUserTypeDao.LookupUserType_Upsert(abstractLookupUserType);
        }

       

        public override PagedList<AbstractLookupUserType> LookupUserType_All(PageParam pageParam, string search)
        {
            return this.abstractLookupUserTypeDao.LookupUserType_All(pageParam, search);

        }
      

       

    }
}
