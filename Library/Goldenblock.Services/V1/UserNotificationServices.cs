﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class UserNotificationServices : AbstractUserNotificationServices
    {
        private AbstractUserNotificationDao abstractUserNotificationDao;

        public UserNotificationServices(AbstractUserNotificationDao abstractUserNotificationDao)
        {
            this.abstractUserNotificationDao = abstractUserNotificationDao;
        }

        public override PagedList<AbstractUserNotification> UserNotification_All(PageParam pageParam, string search, long UserId, long LookupNotificationTypeId)
        {
            return this.abstractUserNotificationDao.UserNotification_All(pageParam, search, UserId, LookupNotificationTypeId);
        }


        public override SuccessResult<AbstractUserNotification> UserNotification_ById(long Id)
        {
            return this.abstractUserNotificationDao.UserNotification_ById(Id);
        }

        public override PagedList<AbstractUserNotification> UserNotification_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractUserNotificationDao.UserNotification_ByUserId(pageParam,  UserId);
        }

        public override SuccessResult<AbstractUserNotification> UserNotification_Upsert(AbstractUserNotification abstractUserNotification)
        {
            return this.abstractUserNotificationDao.UserNotification_Upsert(abstractUserNotification); ;
        }

    }

}