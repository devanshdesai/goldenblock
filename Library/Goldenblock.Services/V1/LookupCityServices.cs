﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupCityServices : AbstractLookupCityServices
    {
        private AbstractLookupCityDao abstractLookupCityDao;

        public LookupCityServices(AbstractLookupCityDao abstractLookupCityDao)
        {
            this.abstractLookupCityDao = abstractLookupCityDao;
        }

        public override SuccessResult<AbstractLookupCity> LookupCity_Upsert(AbstractLookupCity abstractLookupCity)
        {
            return this.abstractLookupCityDao.LookupCity_Upsert(abstractLookupCity);
        }

        public override SuccessResult<AbstractLookupCity> LookupCity_ById(long Id)
        {
            return this.abstractLookupCityDao.LookupCity_ById(Id);
        }

        public override PagedList<AbstractLookupCity> LookupCity_All(PageParam pageParam, string search)
        {
            return this.abstractLookupCityDao.LookupCity_All(pageParam, search);

        }


    }
}
