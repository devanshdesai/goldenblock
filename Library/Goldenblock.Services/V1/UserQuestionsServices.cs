﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class UserQuestionsServices : AbstractUserQuestionsServices
    {
        private AbstractUserQuestionsDao abstractUserQuestionsDao;

        public UserQuestionsServices(AbstractUserQuestionsDao abstractUserQuestionsDao)
        {
            this.abstractUserQuestionsDao = abstractUserQuestionsDao;
        }

        public override PagedList<AbstractUserQuestions> UserQuestions_All(PageParam pageParam, string search,long UserId)
        {
            return this.abstractUserQuestionsDao.UserQuestions_All(pageParam, search,UserId);
        }

       
        public override SuccessResult<AbstractUserQuestions> UserQuestions_ById(long Id)
        {
            return this.abstractUserQuestionsDao.UserQuestions_ById(Id);
        }

       

        public override SuccessResult<AbstractUserQuestions> UserQuestions_Upsert(AbstractUserQuestions abstractUserQuestions)
        {
            return this.abstractUserQuestionsDao.UserQuestions_Upsert(abstractUserQuestions); ;
        }

    }

}