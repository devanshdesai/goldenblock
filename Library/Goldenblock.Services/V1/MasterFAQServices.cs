﻿using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class MasterFAQServices : AbstractMasterFAQServices
    {
        private AbstractMasterFAQDao abstractMasterFAQDao;

        public MasterFAQServices(AbstractMasterFAQDao abstractMasterFAQDao)
        {
            this.abstractMasterFAQDao = abstractMasterFAQDao;
        }

        public override SuccessResult<AbstractMasterFAQ> MasterFAQ_Upsert(AbstractMasterFAQ abstractLookupCity)
        {
            return this.abstractMasterFAQDao.MasterFAQ_Upsert(abstractLookupCity);
        }

        public override SuccessResult<AbstractMasterFAQ> MasterFAQ_ById(long Id)
        {
            return this.abstractMasterFAQDao.MasterFAQ_ById(Id);
        }

        public override PagedList<AbstractMasterFAQ> MasterFAQ_All(PageParam pageParam, string search)
        {
            return this.abstractMasterFAQDao.MasterFAQ_All(pageParam, search);

        }

    }
}
