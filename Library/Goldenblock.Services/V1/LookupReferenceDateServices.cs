﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupReferenceDateServices : AbstractLookupReferenceDateServices
    {
        private AbstractLookupReferenceDateDao abstractLookupReferenceDateDao;

        public LookupReferenceDateServices(AbstractLookupReferenceDateDao abstractLookupReferenceDateDao)
        {
            this.abstractLookupReferenceDateDao = abstractLookupReferenceDateDao;
        }

        public override SuccessResult<AbstractLookupReferenceDate> LookupReferenceDate_Upsert(AbstractLookupReferenceDate abstractLookupReferenceDate)
        {
            return this.abstractLookupReferenceDateDao.LookupReferenceDate_Upsert(abstractLookupReferenceDate);
        }

        public override SuccessResult<AbstractLookupReferenceDate> LookupReferenceDate_ById(long Id)
        {
            return this.abstractLookupReferenceDateDao.LookupReferenceDate_ById(Id);
        }

        public override PagedList<AbstractLookupReferenceDate> LookupReferenceDate_All(PageParam pageParam, string search)
        {
            return this.abstractLookupReferenceDateDao.LookupReferenceDate_All(pageParam, search);

        }


    }
}
