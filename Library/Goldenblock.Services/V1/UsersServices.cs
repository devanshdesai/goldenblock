﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class UsersServices : AbstractUsersServices
    {
        private AbstractUsersDao abstractUsersDao;

        public UsersServices(AbstractUsersDao abstractUsersDao)
        {
            this.abstractUsersDao = abstractUsersDao;
        }

        public override PagedList<AbstractUsers> Users_All(PageParam pageParam, string search , long LookupUserTypeId, long LookupCountryId, long LookupStateId, long LookupCityId, long LookupStatusId)
        {
            return this.abstractUsersDao.Users_All(pageParam, search, LookupUserTypeId, LookupCountryId, LookupStateId, LookupCityId,  LookupStatusId);
        }
       
        public override SuccessResult<AbstractUsers> Users_ById(long Id)
        {
            return this.abstractUsersDao.Users_ById(Id);
        }

        public override SuccessResult<AbstractUsers> Users_Login(string Email, string Password)
        {
            SuccessResult<AbstractUsers> response = this.abstractUsersDao.Users_Login(Email, Password);
           
            return response;
        }       

        public override bool Users_Logout(long Id)
        {
            return this.abstractUsersDao.Users_Logout(Id);
        }
       
        public override SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_Upsert(abstractUsers); ;
        }

        public override SuccessResult<AbstractUsers> Users_Delete(long Id, long DeletedBy)
        {
            return this.abstractUsersDao.Users_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractUsers> Users_IsAgreeTermsAndConditions(long Id)
        {
            return this.abstractUsersDao.Users_IsAgreeTermsAndConditions(Id);
        }

        public override SuccessResult<AbstractUsers> Users_IsEmailVerified(long Id)
        {
            return this.abstractUsersDao.Users_IsEmailVerified(Id );
        }     

        public override SuccessResult<AbstractUsers> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            return this.abstractUsersDao.Users_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword );
        }

        public override SuccessResult<AbstractUsers> Users_UpdateLookupStatusId(long Id, long LookupStatusId, long UpdatedBy)
        {
            return this.abstractUsersDao.Users_UpdateLookupStatusId(Id, LookupStatusId, UpdatedBy);
        }

        public override SuccessResult<AbstractUsers> SendResetPasswordLinkBy_UserEmail(string Email)
        {
            return this.abstractUsersDao.SendResetPasswordLinkBy_UserEmail(Email);
        }

        public override SuccessResult<AbstractUsers> ResetPasswordBy_UserEmail(string Email, string ResetCode, string NewPassword, string ConfirmPassword)
        {
            return this.abstractUsersDao.ResetPasswordBy_UserEmail(Email, ResetCode, NewPassword, ConfirmPassword);
        }
    }

}