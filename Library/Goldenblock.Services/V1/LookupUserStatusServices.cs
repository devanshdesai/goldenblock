﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupUserStatusServices : AbstractLookupUserStatusServices
    {
        private AbstractLookupUserStatusDao abstractLookupUserStatusDao;

        public LookupUserStatusServices(AbstractLookupUserStatusDao abstractLookupUserStatusDao)
        {
            this.abstractLookupUserStatusDao = abstractLookupUserStatusDao;
        }

        public override SuccessResult<AbstractLookupUserStatus> LookupUserStatus_Upsert(AbstractLookupUserStatus abstractLookupUserStatus)
        {
            return this.abstractLookupUserStatusDao.LookupUserStatus_Upsert(abstractLookupUserStatus);
        }



        public override PagedList<AbstractLookupUserStatus> LookupUserStatus_All(PageParam pageParam, string search)
        {
            return this.abstractLookupUserStatusDao.LookupUserStatus_All(pageParam, search);

        }




    }
}
