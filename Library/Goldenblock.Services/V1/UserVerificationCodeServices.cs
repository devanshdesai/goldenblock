﻿using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class UserVerificationCodeServices : AbstractUserVerificationCodeServices
    {
        private AbstractUserVerificationCodeDao abstractUserVerificationCodeDao;

        public UserVerificationCodeServices(AbstractUserVerificationCodeDao abstractUserVerificationCodeDao)
        {
            this.abstractUserVerificationCodeDao = abstractUserVerificationCodeDao;
        }

        public override PagedList<AbstractUserVerificationCode> UserVerificationCode_All(PageParam pageParam, string search, long UsersId)
        {
            return this.abstractUserVerificationCodeDao.UserVerificationCode_All(pageParam, search, UsersId);
        }


        public override SuccessResult<AbstractUserVerificationCode> UserVerificationCode_ById(long Id)
        {
            return this.abstractUserVerificationCodeDao.UserVerificationCode_ById(Id);
        }



        public override SuccessResult<AbstractUserVerificationCode> UserVerificationCode_Upsert(AbstractUserVerificationCode abstractUserVerificationCode)
        {
            return this.abstractUserVerificationCodeDao.UserVerificationCode_Upsert(abstractUserVerificationCode);

        }



        public override SuccessResult<AbstractUserVerificationCode> UserVerificationCode_SendVerificationCode(AbstractUserVerificationCode abstractUserVerificationCode)
        {
            return this.abstractUserVerificationCodeDao.UserVerificationCode_SendVerificationCode(abstractUserVerificationCode); ;
        }

    }

}