﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupCountryServices : AbstractLookupCountryServices
    {
        private AbstractLookupCountryDao abstractLookupCountryDao;

        public LookupCountryServices(AbstractLookupCountryDao abstractLookupCountryDao)
        {
            this.abstractLookupCountryDao = abstractLookupCountryDao;
        }

        public override SuccessResult<AbstractLookupCountry> LookupCountry_Upsert(AbstractLookupCountry abstractLookupCountry)
        {
            return this.abstractLookupCountryDao.LookupCountry_Upsert(abstractLookupCountry);
        }

        public override SuccessResult<AbstractLookupCountry> LookupCountry_ById(long Id)
        {
            return this.abstractLookupCountryDao.LookupCountry_ById(Id);
        }

        public override PagedList<AbstractLookupCountry> LookupCountry_All(PageParam pageParam, string search)
        {
            return this.abstractLookupCountryDao.LookupCountry_All(pageParam, search);

        }


    }
}
