﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class PropertyThingsToConsiderWhenInvestingServices : AbstractPropertyThingsToConsiderWhenInvestingServices
    {
        private AbstractPropertyThingsToConsiderWhenInvestingDao abstractPropertyThingsToConsiderWhenInvestingDao;

        public PropertyThingsToConsiderWhenInvestingServices(AbstractPropertyThingsToConsiderWhenInvestingDao abstractPropertyThingsToConsiderWhenInvestingDao)
        {
            this.abstractPropertyThingsToConsiderWhenInvestingDao = abstractPropertyThingsToConsiderWhenInvestingDao;
        }

        public override PagedList<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_All(PageParam pageParam, string search, long PropertyId)
        {
            return this.abstractPropertyThingsToConsiderWhenInvestingDao.PropertyThingsToConsiderWhenInvesting_All(pageParam, search, PropertyId);
        }

        public override SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_ById(long Id)
        {
            return this.abstractPropertyThingsToConsiderWhenInvestingDao.PropertyThingsToConsiderWhenInvesting_ById(Id);
        }

        public override SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_Delete(long Id, long DeletedBy)
        {
            return this.abstractPropertyThingsToConsiderWhenInvestingDao.PropertyThingsToConsiderWhenInvesting_Delete(Id, DeletedBy);
        }

        public override PagedList<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_PropertyId(PageParam pageParam, long PropertyId)
        {
            return this.abstractPropertyThingsToConsiderWhenInvestingDao.PropertyThingsToConsiderWhenInvesting_PropertyId(pageParam, PropertyId);
        }

        public override SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_Upsert(AbstractPropertyThingsToConsiderWhenInvesting abstractPropertyThingsToConsiderWhenInvesting)
        {
            return this.abstractPropertyThingsToConsiderWhenInvestingDao.PropertyThingsToConsiderWhenInvesting_Upsert(abstractPropertyThingsToConsiderWhenInvesting); ;
        }

    }

}