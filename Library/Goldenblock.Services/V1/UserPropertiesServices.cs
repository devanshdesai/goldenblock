﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class UserPropertiesServices : AbstractUserPropertiesServices
    {
        private AbstractUserPropertiesDao abstractUserPropertiesDao;

        public UserPropertiesServices(AbstractUserPropertiesDao abstractUserPropertiesDao)
        {
            this.abstractUserPropertiesDao = abstractUserPropertiesDao;
        }

        public override PagedList<AbstractUserProperties> UserProperties_All(PageParam pageParam, string search, long PropertyId,long UserId)
        {
            return this.abstractUserPropertiesDao.UserProperties_All(pageParam, search, PropertyId, UserId);
        }

        public override SuccessResult<AbstractUserProperties> UserProperties_ById(long Id)
        {
            return this.abstractUserPropertiesDao.UserProperties_ById(Id);
        }

        public override PagedList<AbstractUserProperties> UserProperties_ByPropertyId(PageParam pageParam, long PropertyId)
        {
            return this.abstractUserPropertiesDao.UserProperties_ByPropertyId(pageParam, PropertyId);
        }

        public override PagedList<AbstractUserProperties> UserProperties_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractUserPropertiesDao.UserProperties_ByPropertyId(pageParam, UserId);
        }

        public override SuccessResult<AbstractUserProperties> UserProperties_Upsert(AbstractUserProperties abstractUserProperties)
        {
            return this.abstractUserPropertiesDao.UserProperties_Upsert(abstractUserProperties); ;
        }

    }

}