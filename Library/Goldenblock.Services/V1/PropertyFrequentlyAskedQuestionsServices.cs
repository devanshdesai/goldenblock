﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;

namespace Goldenblock.Services.V1
{
    public class PropertyFrequentlyAskedQuestionsServices : AbstractPropertyFrequentlyAskedQuestionsServices
    {
        private AbstractPropertyFrequentlyAskedQuestionsDao abstractPropertyFrequentlyAskedQuestionsDao;

        public PropertyFrequentlyAskedQuestionsServices(AbstractPropertyFrequentlyAskedQuestionsDao abstractPropertyFrequentlyAskedQuestionsDao)
        {
            this.abstractPropertyFrequentlyAskedQuestionsDao = abstractPropertyFrequentlyAskedQuestionsDao;
        }

        public override PagedList<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_All(PageParam pageParam, string search,long PropertyId)
        {
            return this.abstractPropertyFrequentlyAskedQuestionsDao.PropertyFrequentlyAskedQuestions_All(pageParam, search, PropertyId);
        }

        public override SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_ById(long Id)
        {
            return this.abstractPropertyFrequentlyAskedQuestionsDao.PropertyFrequentlyAskedQuestions_ById(Id);
        }

        public override SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_Delete(long Id, long DeletedBy)
        {
            return this.abstractPropertyFrequentlyAskedQuestionsDao.PropertyFrequentlyAskedQuestions_Delete(Id, DeletedBy);
        }

        public override PagedList<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_PropertyId(PageParam pageParam, long PropertyId)
        {
            return this.abstractPropertyFrequentlyAskedQuestionsDao.PropertyFrequentlyAskedQuestions_PropertyId(pageParam,  PropertyId);
        }

        public override SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_Upsert(AbstractPropertyFrequentlyAskedQuestions abstractPropertyFrequentlyAskedQuestions)
        {
            return this.abstractPropertyFrequentlyAskedQuestionsDao.PropertyFrequentlyAskedQuestions_Upsert(abstractPropertyFrequentlyAskedQuestions); ;
        }

    }

}