﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Services.Contract;


namespace Goldenblock.Services.V1
{
    public class LookupStateServices : AbstractLookupStateServices
    {
        private AbstractLookupStateDao abstractLookupStateDao;

        public LookupStateServices(AbstractLookupStateDao abstractLookupStateDao)
        {
            this.abstractLookupStateDao = abstractLookupStateDao;
        }

        public override SuccessResult<AbstractLookupState> LookupState_Upsert(AbstractLookupState abstractLookupState)
        {
            return this.abstractLookupStateDao.LookupState_Upsert(abstractLookupState);
        }

        public override SuccessResult<AbstractLookupState> LookupState_ById(long Id)
        {
            return this.abstractLookupStateDao.LookupState_ById(Id);
        }

        public override PagedList<AbstractLookupState> LookupState_All(PageParam pageParam, string search)
        {
            return this.abstractLookupStateDao.LookupState_All(pageParam, search);

        }


    }
}
