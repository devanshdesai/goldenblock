﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractLookupConstructionPeriodServices
    {
        public abstract SuccessResult<AbstractLookupConstructionPeriod> LookupConstructionPeriod_Upsert(AbstractLookupConstructionPeriod abstractLookupConstructionPeriod);

        public abstract SuccessResult<AbstractLookupConstructionPeriod> LookupConstructionPeriod_ById(long Id);
        public abstract PagedList<AbstractLookupConstructionPeriod> LookupConstructionPeriod_All(PageParam pageParam, string search);


    }
}
