﻿using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractMasterFAQServices
    {
        public abstract SuccessResult<AbstractMasterFAQ> MasterFAQ_Upsert(AbstractMasterFAQ abstractMasterFAQ);
        public abstract SuccessResult<AbstractMasterFAQ> MasterFAQ_ById(long Id);
        public abstract PagedList<AbstractMasterFAQ> MasterFAQ_All(PageParam pageParam, string search);
    }
}
