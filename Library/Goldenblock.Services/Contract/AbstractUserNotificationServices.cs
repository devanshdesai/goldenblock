﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractUserNotificationServices
    {
        public abstract PagedList<AbstractUserNotification> UserNotification_All(PageParam pageParam, string search, long UserId, long LookupNotificationTypeId);
        public abstract PagedList<AbstractUserNotification> UserNotification_ByUserId(PageParam pageParam, long UserId);
        public abstract SuccessResult<AbstractUserNotification> UserNotification_ById(long Id);
        public abstract SuccessResult<AbstractUserNotification> UserNotification_Upsert(AbstractUserNotification abstractUserNotification);
    }
}
