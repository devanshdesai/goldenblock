﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractPropertyFrequentlyAskedQuestionsServices
    {
        public abstract PagedList<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_All(PageParam pageParam, string search, long PropertyId);
        public abstract SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_ById(long Id);
        public abstract PagedList<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_PropertyId(PageParam pageParam, long PropertyId);
        public abstract SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_Upsert(AbstractPropertyFrequentlyAskedQuestions abstractPropertyFrequentlyAskedQuestions);
    }
}
