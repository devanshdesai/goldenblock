﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractLookupPropertyStatusServices
    {
        public abstract SuccessResult<AbstractLookupPropertyStatus> LookupPropertyStatus_Upsert(AbstractLookupPropertyStatus abstractLookupPropertyStatus);

        public abstract PagedList<AbstractLookupPropertyStatus> LookupPropertyStatus_All(PageParam pageParam, string search);


    }
}
