﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractPropertyServices
    {
        public abstract PagedList<AbstractProperty> Property_All(PageParam pageParam, string search, long UserId, long LookupConstructionPeriodId, long LookupReferenceDateId, long LookupStatusId);
        public abstract SuccessResult<AbstractProperty> Property_ById(long Id);
        public abstract SuccessResult<AbstractProperty> Property_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractProperty> Property_Upsert(AbstractProperty abstractProperty);
        public abstract SuccessResult<AbstractProperty> Property_UpdateLookupStatusId(long Id, long LookupStatusId, long UpdatedBy);

        public abstract PagedList<AbstractProperty> Property_ByUserId(PageParam pageParam, long UserId);

    }
}
