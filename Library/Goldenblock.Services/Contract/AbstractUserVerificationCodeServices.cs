﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractUserVerificationCodeServices
    {
        public abstract PagedList<AbstractUserVerificationCode> UserVerificationCode_All(PageParam pageParam, string search, long UsersId);
        public abstract SuccessResult<AbstractUserVerificationCode> UserVerificationCode_ById(long Id);
        public abstract SuccessResult<AbstractUserVerificationCode> UserVerificationCode_Upsert(AbstractUserVerificationCode abstractUserVerificationCode);
        public abstract SuccessResult<AbstractUserVerificationCode> UserVerificationCode_SendVerificationCode(AbstractUserVerificationCode abstractUserVerificationCode);
    }
}
