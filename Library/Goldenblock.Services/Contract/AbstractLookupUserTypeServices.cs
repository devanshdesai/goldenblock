﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractLookupUserTypeServices
    {
        public abstract SuccessResult<AbstractLookupUserType> LookupUserType_Upsert(AbstractLookupUserType abstractLookupUserType);
        
        public abstract PagedList<AbstractLookupUserType> LookupUserType_All(PageParam pageParam, string search);
        

    }
}
