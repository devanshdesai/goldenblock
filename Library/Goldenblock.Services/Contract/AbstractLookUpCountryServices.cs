﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractLookupCountryServices
    {
        public abstract SuccessResult<AbstractLookupCountry> LookupCountry_Upsert(AbstractLookupCountry abstractLookupCountry);

        public abstract SuccessResult<AbstractLookupCountry> LookupCountry_ById(long Id);
        public abstract PagedList<AbstractLookupCountry> LookupCountry_All(PageParam pageParam, string search);


    }
}
