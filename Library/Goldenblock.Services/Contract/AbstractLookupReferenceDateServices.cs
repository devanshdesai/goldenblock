﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractLookupReferenceDateServices
    {
        public abstract SuccessResult<AbstractLookupReferenceDate> LookupReferenceDate_Upsert(AbstractLookupReferenceDate abstractLookupReferenceDate);

        public abstract SuccessResult<AbstractLookupReferenceDate> LookupReferenceDate_ById(long Id);
        public abstract PagedList<AbstractLookupReferenceDate> LookupReferenceDate_All(PageParam pageParam, string search);


    }
}
