﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractUserPropertiesServices
    {
        public abstract PagedList<AbstractUserProperties> UserProperties_All(PageParam pageParam, string search, long PropertyId, long UserId);
        public abstract SuccessResult<AbstractUserProperties> UserProperties_ById(long Id);
        public abstract PagedList<AbstractUserProperties> UserProperties_ByPropertyId(PageParam pageParam, long PropertyId);
        public abstract PagedList<AbstractUserProperties> UserProperties_ByUserId(PageParam pageParam, long UserId);

        public abstract SuccessResult<AbstractUserProperties> UserProperties_Upsert(AbstractUserProperties abstractUserProperties);
    }
}
