﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractPropertyThingsToConsiderWhenInvestingServices
    {
        public abstract PagedList<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_All(PageParam pageParam, string search, long PropertyId);
        public abstract SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_ById(long Id);
        public abstract PagedList<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_PropertyId(PageParam pageParam, long PropertyId);
        public abstract SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_Upsert(AbstractPropertyThingsToConsiderWhenInvesting abstractPropertyThingsToConsiderWhenInvesting);
    }
}
