﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Services.Contract
{
    public abstract class AbstractUsersServices
    {
        public abstract SuccessResult<AbstractUsers> Users_IsAgreeTermsAndConditions(long Id);
        public abstract SuccessResult<AbstractUsers> Users_IsEmailVerified(long Id);
        public abstract PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, long LookupUserTypeId, long LookupCountryId, long LookupStateId, long LookupCityId, long LookupStatusId);
        public abstract SuccessResult<AbstractUsers> Users_ById(long Id);
        public abstract SuccessResult<AbstractUsers> Users_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractUsers> Users_Login(string Email, string Password);
        public abstract bool Users_Logout(long Id);
        public abstract SuccessResult<AbstractUsers> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword);
        public abstract SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers);
        public abstract SuccessResult<AbstractUsers> Users_UpdateLookupStatusId(long Id, long LookupStatusId, long UpdatedBy);
        public abstract SuccessResult<AbstractUsers> SendResetPasswordLinkBy_UserEmail(string Email);
        public abstract SuccessResult<AbstractUsers> ResetPasswordBy_UserEmail(string Email, string ResetCode, string NewPassword, string ConfirmPassword);

    }
}
