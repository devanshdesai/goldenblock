﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Goldenblock.Services
{
    using Autofac;
    using Data;
    using Goldenblock.Services.Contract;  



    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DataModule>();
            //builder.RegisterType<V1.LookUpServicesServices>().As<AbstractLookUpServicesServices>().InstancePerDependency();

            builder.RegisterType<V1.LookupUserTypeServices>().As<AbstractLookupUserTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupNotificationTypeServices>().As<AbstractLookupNotificationTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupUserStatusServices>().As<AbstractLookupUserStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterFAQServices>().As<AbstractMasterFAQServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupStateServices>().As<AbstractLookupStateServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupCountryServices>().As<AbstractLookupCountryServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupCityServices>().As<AbstractLookupCityServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupPropertyStatusServices>().As<AbstractLookupPropertyStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupReferenceDateServices>().As<AbstractLookupReferenceDateServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupConstructionPeriodServices>().As<AbstractLookupConstructionPeriodServices>().InstancePerDependency();
            builder.RegisterType<V1.UserVerificationCodeServices>().As<AbstractUserVerificationCodeServices>().InstancePerDependency();
            builder.RegisterType<V1.UserQuestionsServices>().As<AbstractUserQuestionsServices>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationServices>().As<AbstractUserNotificationServices>().InstancePerDependency();
            builder.RegisterType<V1.UsersServices>().As<AbstractUsersServices>().InstancePerDependency();
            builder.RegisterType<V1.PropertyFrequentlyAskedQuestionsServices>().As<AbstractPropertyFrequentlyAskedQuestionsServices>().InstancePerDependency();
            builder.RegisterType<V1.PropertyThingsToConsiderWhenInvestingServices>().As<AbstractPropertyThingsToConsiderWhenInvestingServices>().InstancePerDependency();
            builder.RegisterType<V1.UserPropertiesServices>().As<AbstractUserPropertiesServices>().InstancePerDependency();
            builder.RegisterType<V1.PropertyServices>().As<AbstractPropertyServices>().InstancePerDependency();
            builder.RegisterType<V1.AdminServices>().As<AbstractAdminServices>().InstancePerDependency();


            base.Load(builder);
        }
    }
}
