﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Data.Contract
{
    public abstract class AbstractLookupNotificationTypeDao
    {
        public abstract SuccessResult<AbstractLookupNotificationType> LookupNotificationType_Upsert(AbstractLookupNotificationType abstractLookupNotificationType);

        public abstract PagedList<AbstractLookupNotificationType> LookupNotificationType_All(PageParam pageParam, string search);


    }
}
