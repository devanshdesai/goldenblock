﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Data.Contract
{
    public abstract class AbstractLookupUserStatusDao
    {
        public abstract SuccessResult<AbstractLookupUserStatus> LookupUserStatus_Upsert(AbstractLookupUserStatus abstractLookupUserStatus);

        public abstract PagedList<AbstractLookupUserStatus> LookupUserStatus_All(PageParam pageParam, string search);


    }
}
