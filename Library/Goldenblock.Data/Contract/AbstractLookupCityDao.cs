﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Data.Contract
{
    public abstract class AbstractLookupCityDao
    {
        public abstract SuccessResult<AbstractLookupCity> LookupCity_Upsert(AbstractLookupCity abstractLookupCity);
        public abstract SuccessResult<AbstractLookupCity> LookupCity_ById(long Id);
        public abstract PagedList<AbstractLookupCity> LookupCity_All(PageParam pageParam, string search);

    }
}
