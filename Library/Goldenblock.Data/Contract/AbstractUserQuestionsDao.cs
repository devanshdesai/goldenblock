﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Data.Contract
{
    public abstract class AbstractUserQuestionsDao : AbstractBaseDao
    {
        
        public abstract PagedList<AbstractUserQuestions> UserQuestions_All(PageParam pageParam, string search,long UserId);
        public abstract SuccessResult<AbstractUserQuestions> UserQuestions_ById(long Id);
       public abstract SuccessResult<AbstractUserQuestions> UserQuestions_Upsert(AbstractUserQuestions abstractUserQuestions);
    }
}
