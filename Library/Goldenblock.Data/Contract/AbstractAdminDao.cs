﻿using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Entities.Contract;

namespace Goldenblock.Data.Contract
{
    public abstract class AbstractAdminDao
    {
        public abstract SuccessResult<AbstractAdmins> Admin_Login(string Email, string Password);

    }
}