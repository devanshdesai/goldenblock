﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class LookupReferenceDateDao : AbstractLookupReferenceDateDao
    {

        public override SuccessResult<AbstractLookupReferenceDate> LookupReferenceDate_Upsert(AbstractLookupReferenceDate abstractLookupReferenceDate)
        {
            SuccessResult<AbstractLookupReferenceDate> LookupReferenceDate = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupReferenceDate.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupReferenceDate.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupReferenceDate.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupReferenceDate.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupReferenceDate_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupReferenceDate = task.Read<SuccessResult<AbstractLookupReferenceDate>>().SingleOrDefault();
                LookupReferenceDate.Item = task.Read<LookupReferenceDate>().SingleOrDefault();
            }

            return LookupReferenceDate;
        }

        public override SuccessResult<AbstractLookupReferenceDate> LookupReferenceDate_ById(long Id)
        {
            SuccessResult<AbstractLookupReferenceDate> LookupReferenceDate = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupReferenceDate_ById, param, commandType: CommandType.StoredProcedure);
                LookupReferenceDate = task.Read<SuccessResult<AbstractLookupReferenceDate>>().SingleOrDefault();
                LookupReferenceDate.Item = task.Read<LookupReferenceDate>().SingleOrDefault();
            }

            return LookupReferenceDate;
        }

        public override PagedList<AbstractLookupReferenceDate> LookupReferenceDate_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupReferenceDate> LookupReferenceDate = new PagedList<AbstractLookupReferenceDate>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupReferenceDate_All, param, commandType: CommandType.StoredProcedure);
                LookupReferenceDate.Values.AddRange(task.Read<LookupReferenceDate>());
                LookupReferenceDate.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupReferenceDate;
        }




    }
}
