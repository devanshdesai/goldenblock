﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class LookupCountryDao : AbstractLookupCountryDao
    {

        public override SuccessResult<AbstractLookupCountry> LookupCountry_Upsert(AbstractLookupCountry abstractLookupCountry)
        {
            SuccessResult<AbstractLookupCountry> LookupCountry = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupCountry.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CountryCode", abstractLookupCountry.CountryCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupCountry.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupCountry.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupCountry.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupCountry_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupCountry = task.Read<SuccessResult<AbstractLookupCountry>>().SingleOrDefault();
                LookupCountry.Item = task.Read<LookupCountry>().SingleOrDefault();
            }

            return LookupCountry;
        }

        public override SuccessResult<AbstractLookupCountry> LookupCountry_ById(long Id)
        {
            SuccessResult<AbstractLookupCountry> LookupCountry = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupCountry_ById, param, commandType: CommandType.StoredProcedure);
                LookupCountry = task.Read<SuccessResult<AbstractLookupCountry>>().SingleOrDefault();
                LookupCountry.Item = task.Read<LookupCountry>().SingleOrDefault();
            }

            return LookupCountry;
        }

        public override PagedList<AbstractLookupCountry> LookupCountry_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupCountry> LookupCountry = new PagedList<AbstractLookupCountry>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupCountry_All, param, commandType: CommandType.StoredProcedure);
                LookupCountry.Values.AddRange(task.Read<LookupCountry>());
                LookupCountry.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupCountry;
        }




    }
}
