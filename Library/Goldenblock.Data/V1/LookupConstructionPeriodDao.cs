﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class LookupConstructionPeriodDao : AbstractLookupConstructionPeriodDao
    {

        public override SuccessResult<AbstractLookupConstructionPeriod> LookupConstructionPeriod_Upsert(AbstractLookupConstructionPeriod abstractLookupConstructionPeriod)
        {
            SuccessResult<AbstractLookupConstructionPeriod> LookupConstructionPeriod = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupConstructionPeriod.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupConstructionPeriod.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupConstructionPeriod.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupConstructionPeriod.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupConstructionPeriod_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupConstructionPeriod = task.Read<SuccessResult<AbstractLookupConstructionPeriod>>().SingleOrDefault();
                LookupConstructionPeriod.Item = task.Read<LookupConstructionPeriod>().SingleOrDefault();
            }

            return LookupConstructionPeriod;
        }

        public override SuccessResult<AbstractLookupConstructionPeriod> LookupConstructionPeriod_ById(long Id)
        {
            SuccessResult<AbstractLookupConstructionPeriod> LookupConstructionPeriod = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupConstructionPeriod_ById, param, commandType: CommandType.StoredProcedure);
                LookupConstructionPeriod = task.Read<SuccessResult<AbstractLookupConstructionPeriod>>().SingleOrDefault();
                LookupConstructionPeriod.Item = task.Read<LookupConstructionPeriod>().SingleOrDefault();
            }

            return LookupConstructionPeriod;
        }

        public override PagedList<AbstractLookupConstructionPeriod> LookupConstructionPeriod_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupConstructionPeriod> LookupConstructionPeriod = new PagedList<AbstractLookupConstructionPeriod>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupConstructionPeriod_All, param, commandType: CommandType.StoredProcedure);
                LookupConstructionPeriod.Values.AddRange(task.Read<LookupConstructionPeriod>());
                LookupConstructionPeriod.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupConstructionPeriod;
        }




    }
}
