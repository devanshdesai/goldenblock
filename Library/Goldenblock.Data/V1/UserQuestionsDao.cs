﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class UserQuestionsDao : AbstractUserQuestionsDao
    {
       
        public override PagedList<AbstractUserQuestions> UserQuestions_All(PageParam pageParam, string search, long UserId)
        {
            PagedList<AbstractUserQuestions> UserQuestions = new PagedList<AbstractUserQuestions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserQuestions_All, param, commandType: CommandType.StoredProcedure);
                UserQuestions.Values.AddRange(task.Read<UserQuestions>());
                UserQuestions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserQuestions;
        }
        public override SuccessResult<AbstractUserQuestions> UserQuestions_ById(long Id)
        {
            SuccessResult<AbstractUserQuestions> UserQuestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserQuestions_ById, param, commandType: CommandType.StoredProcedure);
                UserQuestions = task.Read<SuccessResult<AbstractUserQuestions>>().SingleOrDefault();
                UserQuestions.Item = task.Read<UserQuestions>().SingleOrDefault();
            }

            return UserQuestions;
        }
       
       
       
        public override SuccessResult<AbstractUserQuestions> UserQuestions_Upsert(AbstractUserQuestions abstractUserQuestions)
        {
            SuccessResult<AbstractUserQuestions> UserQuestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserQuestions.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserQuestions.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Question", abstractUserQuestions.Question, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractUserQuestions.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserQuestions.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserQuestions.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserQuestions_Upsert, param, commandType: CommandType.StoredProcedure);
                UserQuestions = task.Read<SuccessResult<AbstractUserQuestions>>().SingleOrDefault();
                UserQuestions.Item = task.Read<UserQuestions>().SingleOrDefault();
            }

            return UserQuestions;
        }

    }
}
