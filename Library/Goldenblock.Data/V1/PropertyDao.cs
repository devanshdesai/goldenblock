﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class PropertyDao : AbstractPropertyDao
    {

        public override PagedList<AbstractProperty> Property_All(PageParam pageParam, string search, long UserId, long LookupConstructionPeriodId, long LookupReferenceDateId, long LookupStatusId)
        {
            PagedList<AbstractProperty> Property = new PagedList<AbstractProperty>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupConstructionPeriodId", LookupConstructionPeriodId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupReferenceDateId", LookupReferenceDateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupStatusId", LookupStatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Property_All, param, commandType: CommandType.StoredProcedure);
                Property.Values.AddRange(task.Read<Property>());
                Property.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Property;
        }
        public override SuccessResult<AbstractProperty> Property_ById(long Id)
        {
            SuccessResult<AbstractProperty> Property = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Property_ById, param, commandType: CommandType.StoredProcedure);
                Property = task.Read<SuccessResult<AbstractProperty>>().SingleOrDefault();
                Property.Item = task.Read<Property>().SingleOrDefault();




            }

            return Property;
        }
        public override SuccessResult<AbstractProperty> Property_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractProperty> Property = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Property_Delete, param, commandType: CommandType.StoredProcedure);
                Property = task.Read<SuccessResult<AbstractProperty>>().SingleOrDefault();
                Property.Item = task.Read<Property>().SingleOrDefault();
            }
            return Property;
        }
        public override SuccessResult<AbstractProperty> Property_Upsert(AbstractProperty abstractProperty)
        {
            SuccessResult<AbstractProperty> Property = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProperty.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractProperty.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Title", abstractProperty.Title, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupConstructionPeriodId", abstractProperty.LookupConstructionPeriodId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AnnualRateOfReturn", abstractProperty.AnnualRateOfReturn, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@TotalRecruitmentAmount", abstractProperty.TotalRecruitmentAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@InvestmentAmount", abstractProperty.InvestmentAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@InvestmentPeriod", abstractProperty.InvestmentPeriod, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ExpectedProfit", abstractProperty.ExpectedProfit, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@InvestorProtection", abstractProperty.InvestorProtection, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BusinessAddress", abstractProperty.BusinessAddress, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BusinessName", abstractProperty.BusinessName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BusinessScale", abstractProperty.BusinessScale, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UseOfFunds", abstractProperty.UseOfFunds, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@LTV", abstractProperty.LTV, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@RepaymentOrder", abstractProperty.RepaymentOrder, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@FairRate", abstractProperty.FairRate, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@LookupReferenceDateId", abstractProperty.LookupReferenceDateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SeniorLoan", abstractProperty.SeniorLoan, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ErnestFundLoans", abstractProperty.ErnestFundLoans, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@OtherAmount", abstractProperty.OtherAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ResidualAmountOfCollateralValue", abstractProperty.ResidualAmountOfCollateralValue, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@RepaymentScenario", abstractProperty.RepaymentScenario, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MajorRisk", abstractProperty.MajorRisk, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DetailedAnalysis", abstractProperty.DetailedAnalysis, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupStatusId", abstractProperty.LookupStatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ImageURL1", abstractProperty.ImageURL1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ImageURL2", abstractProperty.ImageURL2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ImageURL3", abstractProperty.ImageURL3, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ImageURL4", abstractProperty.ImageURL4, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PropertyInvestmentStatusId", abstractProperty.PropertyInvestmentStatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProperty.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractProperty.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Property_Upsert, param, commandType: CommandType.StoredProcedure);
                Property = task.Read<SuccessResult<AbstractProperty>>().SingleOrDefault();
                Property.Item = task.Read<Property>().SingleOrDefault();
            }

            return Property;
        }

        public override PagedList<AbstractProperty> Property_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractProperty> Property = new PagedList<AbstractProperty>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Property_ByUserId, param, commandType: CommandType.StoredProcedure);
                Property.Values.AddRange(task.Read<Property>());
                Property.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Property;
        }
        public override SuccessResult<AbstractProperty> Property_UpdateLookupStatusId(long Id, long LookupStatusId, long UpdatedBy)
        {
            SuccessResult<AbstractProperty> Property = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupStatusId", LookupStatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Property_UpdateLookupStatusId, param, commandType: CommandType.StoredProcedure);
                Property = task.Read<SuccessResult<AbstractProperty>>().SingleOrDefault();
                Property.Item = task.Read<Property>().SingleOrDefault();
            }

            return Property;
        }
    }
}
