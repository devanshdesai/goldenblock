﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class PropertyFrequentlyAskedQuestionsDao : AbstractPropertyFrequentlyAskedQuestionsDao
    {
        
        public override PagedList<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_All(PageParam pageParam, string search,long PropertyId)
        {
            PagedList<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions = new PagedList<AbstractPropertyFrequentlyAskedQuestions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PropertyId", PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyFrequentlyAskedQuestions_All, param, commandType: CommandType.StoredProcedure);
                PropertyFrequentlyAskedQuestions.Values.AddRange(task.Read<PropertyFrequentlyAskedQuestions>());
                PropertyFrequentlyAskedQuestions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return PropertyFrequentlyAskedQuestions;
        }
        public override SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_ById(long Id)
        {
            SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyFrequentlyAskedQuestions_ById, param, commandType: CommandType.StoredProcedure);
                PropertyFrequentlyAskedQuestions = task.Read<SuccessResult<AbstractPropertyFrequentlyAskedQuestions>>().SingleOrDefault();
                PropertyFrequentlyAskedQuestions.Item = task.Read<PropertyFrequentlyAskedQuestions>().SingleOrDefault();
            }

            return PropertyFrequentlyAskedQuestions;
        }

        public override SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyFrequentlyAskedQuestions_Delete, param, commandType: CommandType.StoredProcedure);
                PropertyFrequentlyAskedQuestions = task.Read<SuccessResult<AbstractPropertyFrequentlyAskedQuestions>>().SingleOrDefault();
                PropertyFrequentlyAskedQuestions.Item = task.Read<PropertyFrequentlyAskedQuestions>().SingleOrDefault();
            }
            return PropertyFrequentlyAskedQuestions;
        }

        public override PagedList<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_PropertyId(PageParam pageParam, long PropertyId)
        {
            PagedList<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions = new PagedList<AbstractPropertyFrequentlyAskedQuestions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
           
            param.Add("@PropertyId", PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyFrequentlyAskedQuestions_PropertyId, param, commandType: CommandType.StoredProcedure);
                PropertyFrequentlyAskedQuestions.Values.AddRange(task.Read<PropertyFrequentlyAskedQuestions>());
                PropertyFrequentlyAskedQuestions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return PropertyFrequentlyAskedQuestions;
        }

        public override SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions_Upsert(AbstractPropertyFrequentlyAskedQuestions abstractPropertyFrequentlyAskedQuestions)
        {
            SuccessResult<AbstractPropertyFrequentlyAskedQuestions> PropertyFrequentlyAskedQuestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPropertyFrequentlyAskedQuestions.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PropertyId", abstractPropertyFrequentlyAskedQuestions.PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Question", abstractPropertyFrequentlyAskedQuestions.Question, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Answer", abstractPropertyFrequentlyAskedQuestions.Answer, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPropertyFrequentlyAskedQuestions.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractPropertyFrequentlyAskedQuestions.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyFrequentlyAskedQuestions_Upsert, param, commandType: CommandType.StoredProcedure);
                PropertyFrequentlyAskedQuestions = task.Read<SuccessResult<AbstractPropertyFrequentlyAskedQuestions>>().SingleOrDefault();
                PropertyFrequentlyAskedQuestions.Item = task.Read<PropertyFrequentlyAskedQuestions>().SingleOrDefault();
            }

            return PropertyFrequentlyAskedQuestions;
        }

    }
}
