﻿using Dapper;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Goldenblock.Data.V1
{
    public class MasterFAQDao : AbstractMasterFAQDao
    {
        public override SuccessResult<AbstractMasterFAQ> MasterFAQ_Upsert(AbstractMasterFAQ abstractMasterFAQ)
        {
            SuccessResult<AbstractMasterFAQ> MasterFAQ = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterFAQ.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Question", abstractMasterFAQ.Question, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Answer", abstractMasterFAQ.Answer, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractMasterFAQ.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractMasterFAQ.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterFAQ_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterFAQ = task.Read<SuccessResult<AbstractMasterFAQ>>().SingleOrDefault();
                MasterFAQ.Item = task.Read<MasterFAQ>().SingleOrDefault();
            }

            return MasterFAQ;
        }

        public override SuccessResult<AbstractMasterFAQ> MasterFAQ_ById(long Id)
        {
            SuccessResult<AbstractMasterFAQ> MasterFAQ = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterFAQ_ById, param, commandType: CommandType.StoredProcedure);
                MasterFAQ = task.Read<SuccessResult<AbstractMasterFAQ>>().SingleOrDefault();
                MasterFAQ.Item = task.Read<MasterFAQ>().SingleOrDefault();
            }

            return MasterFAQ;
        }

        public override PagedList<AbstractMasterFAQ> MasterFAQ_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterFAQ> MasterFAQ = new PagedList<AbstractMasterFAQ>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterFAQ_All, param, commandType: CommandType.StoredProcedure);
                MasterFAQ.Values.AddRange(task.Read<MasterFAQ>());
                MasterFAQ.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterFAQ;
        }

    }
}
