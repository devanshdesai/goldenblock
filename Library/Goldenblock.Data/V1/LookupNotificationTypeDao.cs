﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class LookupNotificationTypeDao : AbstractLookupNotificationTypeDao
    {

        public override SuccessResult<AbstractLookupNotificationType> LookupNotificationType_Upsert(AbstractLookupNotificationType abstractLookupNotificationType)
        {
            SuccessResult<AbstractLookupNotificationType> LookupNotificationType = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupNotificationType.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupNotificationType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupNotificationType.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupNotificationType.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupNotificationType_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupNotificationType = task.Read<SuccessResult<AbstractLookupNotificationType>>().SingleOrDefault();
                LookupNotificationType.Item = task.Read<LookupNotificationType>().SingleOrDefault();
            }

            return LookupNotificationType;
        }



        public override PagedList<AbstractLookupNotificationType> LookupNotificationType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupNotificationType> LookupNotificationType = new PagedList<AbstractLookupNotificationType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupNotificationType_All, param, commandType: CommandType.StoredProcedure);
                LookupNotificationType.Values.AddRange(task.Read<LookupNotificationType>());
                LookupNotificationType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupNotificationType;
        }


    }
}
