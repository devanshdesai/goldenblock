﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;
using System.Net.Mail;

namespace Goldenblock.Data.V1
{
    public class UserVerificationCodeDao : AbstractUserVerificationCodeDao
    {

        public override PagedList<AbstractUserVerificationCode> UserVerificationCode_All(PageParam pageParam, string search, long UsersId)
        {
            PagedList<AbstractUserVerificationCode> UserVerificationCode = new PagedList<AbstractUserVerificationCode>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UsersId", UsersId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserVerificationCode_All, param, commandType: CommandType.StoredProcedure);
                UserVerificationCode.Values.AddRange(task.Read<UserVerificationCode>());
                UserVerificationCode.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserVerificationCode;
        }
        public override SuccessResult<AbstractUserVerificationCode> UserVerificationCode_ById(long Id)
        {
            SuccessResult<AbstractUserVerificationCode> UserVerificationCode = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserVerificationCode_ById, param, commandType: CommandType.StoredProcedure);
                UserVerificationCode = task.Read<SuccessResult<AbstractUserVerificationCode>>().SingleOrDefault();
                UserVerificationCode.Item = task.Read<UserVerificationCode>().SingleOrDefault();
            }

            return UserVerificationCode;
        }



        public override SuccessResult<AbstractUserVerificationCode> UserVerificationCode_Upsert(AbstractUserVerificationCode abstractUserVerificationCode)
        {
            SuccessResult<AbstractUserVerificationCode> UserVerificationCode = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserVerificationCode.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UsersId", abstractUserVerificationCode.UsersId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Email", abstractUserVerificationCode.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EmailVerificationCode", abstractUserVerificationCode.EmailVerificationCode, dbType: DbType.String, direction: ParameterDirection.Input);

            param.Add("@CreatedBy", abstractUserVerificationCode.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserVerificationCode.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserVerificationCode_Upsert, param, commandType: CommandType.StoredProcedure);
                UserVerificationCode = task.Read<SuccessResult<AbstractUserVerificationCode>>().SingleOrDefault();
                UserVerificationCode.Item = task.Read<UserVerificationCode>().SingleOrDefault();
            }

            //UserVerificationCode.Item.EmailVerificationCode

            try
            {
                var myMessage__ = new SendGrid.SendGridMessage();

                myMessage__.Text = "GoldenBlock Verification Code : " + UserVerificationCode.Item.EmailVerificationCode; ;
                myMessage__.Html = "GoldenBlock Verification Code : " + UserVerificationCode.Item.EmailVerificationCode;

                myMessage__.Subject = "GoldenBlock Verification Code";


                myMessage__.AddTo(UserVerificationCode.Item.Email);

                var transportWeb__ = new SendGrid.Web("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");  // rushkar send Grid API
                myMessage__.From = new MailAddress("no-reply@rushkar.com", "Do Not Reply - GoldenBlock");

                Task.Run(() => transportWeb__.DeliverAsync(myMessage__).Wait());

                //transportWeb__.DeliverAsync(myMessage__).Wait();
            }
            catch (Exception ex)
            {

            }
            return UserVerificationCode;
        }

        public override SuccessResult<AbstractUserVerificationCode> UserVerificationCode_SendVerificationCode(AbstractUserVerificationCode abstractUserVerificationCode)
        {
            SuccessResult<AbstractUserVerificationCode> UserVerificationCode = null;
            var param = new DynamicParameters();

            param.Add("@Email", abstractUserVerificationCode.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EnterEmailVerificationCode", abstractUserVerificationCode.EnterEmailVerificationCode, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserVerificationCode_SendVerificationCode, param, commandType: CommandType.StoredProcedure);
                UserVerificationCode = task.Read<SuccessResult<AbstractUserVerificationCode>>().SingleOrDefault();
                UserVerificationCode.Item = task.Read<UserVerificationCode>().SingleOrDefault();
            }

            return UserVerificationCode;
        }

    }
}
