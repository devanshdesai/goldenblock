﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class LookupPropertyStatusDao : AbstractLookupPropertyStatusDao
    {

        public override SuccessResult<AbstractLookupPropertyStatus> LookupPropertyStatus_Upsert(AbstractLookupPropertyStatus abstractLookupPropertyStatus)
        {
            SuccessResult<AbstractLookupPropertyStatus> LookupPropertyStatus = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupPropertyStatus.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupPropertyStatus.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupPropertyStatus.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupPropertyStatus.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupPropertyStatus_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupPropertyStatus = task.Read<SuccessResult<AbstractLookupPropertyStatus>>().SingleOrDefault();
                LookupPropertyStatus.Item = task.Read<LookupPropertyStatus>().SingleOrDefault();
            }

            return LookupPropertyStatus;
        }



        public override PagedList<AbstractLookupPropertyStatus> LookupPropertyStatus_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupPropertyStatus> LookupPropertyStatus = new PagedList<AbstractLookupPropertyStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupPropertyStatus_All, param, commandType: CommandType.StoredProcedure);
                LookupPropertyStatus.Values.AddRange(task.Read<LookupPropertyStatus>());
                LookupPropertyStatus.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupPropertyStatus;
        }


    }
}
