﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class LookupUserStatusDao : AbstractLookupUserStatusDao
    {

        public override SuccessResult<AbstractLookupUserStatus> LookupUserStatus_Upsert(AbstractLookupUserStatus abstractLookupUserStatus)
        {
            SuccessResult<AbstractLookupUserStatus> LookupUserStatus = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupUserStatus.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupUserStatus.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupUserStatus.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupUserStatus.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupUserStatus_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupUserStatus = task.Read<SuccessResult<AbstractLookupUserStatus>>().SingleOrDefault();
                LookupUserStatus.Item = task.Read<LookupUserStatus>().SingleOrDefault();
            }

            return LookupUserStatus;
        }



        public override PagedList<AbstractLookupUserStatus> LookupUserStatus_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupUserStatus> LookupUserStatus = new PagedList<AbstractLookupUserStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupUserStatus_All, param, commandType: CommandType.StoredProcedure);
                LookupUserStatus.Values.AddRange(task.Read<LookupUserStatus>());
                LookupUserStatus.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupUserStatus;
        }


    }
}
