﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class LookupUserTypeDao : AbstractLookupUserTypeDao
    {

        public override SuccessResult<AbstractLookupUserType> LookupUserType_Upsert(AbstractLookupUserType abstractLookupUserType)
        {
            SuccessResult<AbstractLookupUserType> LookupUserType = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupUserType.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupUserType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupUserType.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupUserType.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupUserType_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupUserType = task.Read<SuccessResult<AbstractLookupUserType>>().SingleOrDefault();
                LookupUserType.Item = task.Read<LookupUserType>().SingleOrDefault();
            }

            return LookupUserType;
        }

       

        public override PagedList<AbstractLookupUserType> LookupUserType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupUserType> LookupUserType = new PagedList<AbstractLookupUserType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupUserType_All, param, commandType: CommandType.StoredProcedure);
                LookupUserType.Values.AddRange(task.Read<LookupUserType>());
                LookupUserType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupUserType;
        }


    }
}
