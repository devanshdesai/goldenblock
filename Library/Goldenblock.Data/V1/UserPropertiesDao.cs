﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class UserPropertiesDao : AbstractUserPropertiesDao
    {

        public override PagedList<AbstractUserProperties> UserProperties_All(PageParam pageParam, string search, long PropertyId,long UserId)
        {
            PagedList<AbstractUserProperties> UserProperties = new PagedList<AbstractUserProperties>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PropertyId", PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserProperties_All, param, commandType: CommandType.StoredProcedure);
                UserProperties.Values.AddRange(task.Read<UserProperties>());
                UserProperties.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserProperties;
        }
        public override SuccessResult<AbstractUserProperties> UserProperties_ById(long Id)
        {
            SuccessResult<AbstractUserProperties> UserProperties = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserProperties_ById, param, commandType: CommandType.StoredProcedure);
                UserProperties = task.Read<SuccessResult<AbstractUserProperties>>().SingleOrDefault();
                UserProperties.Item = task.Read<UserProperties>().SingleOrDefault();
            }

            return UserProperties;
        }

        public override PagedList<AbstractUserProperties> UserProperties_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserProperties> UserProperties = new PagedList<AbstractUserProperties>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserProperties_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserProperties.Values.AddRange(task.Read<UserProperties>());
                UserProperties.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserProperties;
        }

        public override PagedList<AbstractUserProperties> UserProperties_ByPropertyId(PageParam pageParam, long PropertyId)
        {
            PagedList<AbstractUserProperties> UserProperties = new PagedList<AbstractUserProperties>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            param.Add("@PropertyId", PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserProperties_ByPropertyId, param, commandType: CommandType.StoredProcedure);
                UserProperties.Values.AddRange(task.Read<UserProperties>());
                UserProperties.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserProperties;
        }

        public override SuccessResult<AbstractUserProperties> UserProperties_Upsert(AbstractUserProperties abstractUserProperties)
        {
            SuccessResult<AbstractUserProperties> UserProperties = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserProperties.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PropertyId", abstractUserProperties.PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserProperties.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserProperties.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserProperties.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserProperties_Upsert, param, commandType: CommandType.StoredProcedure);
                UserProperties = task.Read<SuccessResult<AbstractUserProperties>>().SingleOrDefault();
                UserProperties.Item = task.Read<UserProperties>().SingleOrDefault();
            }

            return UserProperties;
        }

    }
}
