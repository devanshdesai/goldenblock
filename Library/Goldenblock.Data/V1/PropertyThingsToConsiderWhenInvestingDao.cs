﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class PropertyThingsToConsiderWhenInvestingDao : AbstractPropertyThingsToConsiderWhenInvestingDao
    {

        public override PagedList<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_All(PageParam pageParam, string search, long PropertyId)
        {
            PagedList<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting = new PagedList<AbstractPropertyThingsToConsiderWhenInvesting>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PropertyId", PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyThingsToConsiderWhenInvesting_All, param, commandType: CommandType.StoredProcedure);
                PropertyThingsToConsiderWhenInvesting.Values.AddRange(task.Read<PropertyThingsToConsiderWhenInvesting>());
                PropertyThingsToConsiderWhenInvesting.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return PropertyThingsToConsiderWhenInvesting;
        }
        public override SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_ById(long Id)
        {
            SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyThingsToConsiderWhenInvesting_ById, param, commandType: CommandType.StoredProcedure);
                PropertyThingsToConsiderWhenInvesting = task.Read<SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting>>().SingleOrDefault();
                PropertyThingsToConsiderWhenInvesting.Item = task.Read<PropertyThingsToConsiderWhenInvesting>().SingleOrDefault();
            }

            return PropertyThingsToConsiderWhenInvesting;
        }

        public override SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyThingsToConsiderWhenInvesting_Delete, param, commandType: CommandType.StoredProcedure);
                PropertyThingsToConsiderWhenInvesting = task.Read<SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting>>().SingleOrDefault();
                PropertyThingsToConsiderWhenInvesting.Item = task.Read<PropertyThingsToConsiderWhenInvesting>().SingleOrDefault();
            }
            return PropertyThingsToConsiderWhenInvesting;
        }

        public override PagedList<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_PropertyId(PageParam pageParam, long PropertyId)
        {
            PagedList<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting = new PagedList<AbstractPropertyThingsToConsiderWhenInvesting>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            param.Add("@PropertyId", PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyThingsToConsiderWhenInvesting_PropertyId, param, commandType: CommandType.StoredProcedure);
                PropertyThingsToConsiderWhenInvesting.Values.AddRange(task.Read<PropertyThingsToConsiderWhenInvesting>());
                PropertyThingsToConsiderWhenInvesting.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return PropertyThingsToConsiderWhenInvesting;
        }

        public override SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting_Upsert(AbstractPropertyThingsToConsiderWhenInvesting abstractPropertyThingsToConsiderWhenInvesting)
        {
            SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting> PropertyThingsToConsiderWhenInvesting = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPropertyThingsToConsiderWhenInvesting.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PropertyId", abstractPropertyThingsToConsiderWhenInvesting.PropertyId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Question", abstractPropertyThingsToConsiderWhenInvesting.Question, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Answer", abstractPropertyThingsToConsiderWhenInvesting.Answer, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPropertyThingsToConsiderWhenInvesting.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractPropertyThingsToConsiderWhenInvesting.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PropertyThingsToConsiderWhenInvesting_Upsert, param, commandType: CommandType.StoredProcedure);
                PropertyThingsToConsiderWhenInvesting = task.Read<SuccessResult<AbstractPropertyThingsToConsiderWhenInvesting>>().SingleOrDefault();
                PropertyThingsToConsiderWhenInvesting.Item = task.Read<PropertyThingsToConsiderWhenInvesting>().SingleOrDefault();
            }

            return PropertyThingsToConsiderWhenInvesting;
        }

    }
}
