﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class UserNotificationDao : AbstractUserNotificationDao
    {
        
        public override PagedList<AbstractUserNotification> UserNotification_All(PageParam pageParam, string search,long UserId, long LookupNotificationTypeId)
        {
            PagedList<AbstractUserNotification> UserNotification = new PagedList<AbstractUserNotification>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupNotificationTypeId", LookupNotificationTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotification_All, param, commandType: CommandType.StoredProcedure);
                UserNotification.Values.AddRange(task.Read<UserNotification>());
                UserNotification.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserNotification;
        }
        public override SuccessResult<AbstractUserNotification> UserNotification_ById(long Id)
        {
            SuccessResult<AbstractUserNotification> UserNotification = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotification_ById, param, commandType: CommandType.StoredProcedure);
                UserNotification = task.Read<SuccessResult<AbstractUserNotification>>().SingleOrDefault();
                UserNotification.Item = task.Read<UserNotification>().SingleOrDefault();
            }

            return UserNotification;
        }

        public override PagedList<AbstractUserNotification> UserNotification_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserNotification> UserNotification = new PagedList<AbstractUserNotification>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);           
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotification_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserNotification.Values.AddRange(task.Read<UserNotification>());
                UserNotification.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserNotification;
        }

        public override SuccessResult<AbstractUserNotification> UserNotification_Upsert(AbstractUserNotification abstractUserNotification)
        {
            SuccessResult<AbstractUserNotification> UserNotification = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserNotification.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserNotification.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupNotificationTypeId", abstractUserNotification.LookupNotificationTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Title", abstractUserNotification.Title, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractUserNotification.Description, dbType: DbType.String, direction: ParameterDirection.Input);            
            param.Add("@CreatedBy", abstractUserNotification.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserNotification.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserNotification_Upsert, param, commandType: CommandType.StoredProcedure);
                UserNotification = task.Read<SuccessResult<AbstractUserNotification>>().SingleOrDefault();
                UserNotification.Item = task.Read<UserNotification>().SingleOrDefault();
            }

            return UserNotification;
        }

    }
}
