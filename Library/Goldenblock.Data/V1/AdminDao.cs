﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{   
    public class AdminDao : AbstractAdminDao
    {
        public override SuccessResult<AbstractAdmins> Admin_Login(string Email, string Password)
        {
            SuccessResult<AbstractAdmins> Users = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", Password, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Login, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractAdmins>>().SingleOrDefault();
                Users.Item = task.Read<Admins>().SingleOrDefault();
            }

            return Users;
        }
    }
}
