﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goldenblock.Common;
using Goldenblock.Common.Paging;
using Goldenblock.Data.Contract;
using Goldenblock.Entities.Contract;
using Goldenblock.Entities.V1;
using Dapper;

namespace Goldenblock.Data.V1
{
    public class LookupCityDao : AbstractLookupCityDao
    {

        public override SuccessResult<AbstractLookupCity> LookupCity_Upsert(AbstractLookupCity abstractLookupCity)
        {
            SuccessResult<AbstractLookupCity> LookupCity = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupCity.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupCity.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupCity.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupCity.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupCity_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupCity = task.Read<SuccessResult<AbstractLookupCity>>().SingleOrDefault();
                LookupCity.Item = task.Read<LookupCity>().SingleOrDefault();
            }

            return LookupCity;
        }

        public override SuccessResult<AbstractLookupCity> LookupCity_ById(long Id)
        {
            SuccessResult<AbstractLookupCity> LookupCity = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupCity_ById, param, commandType: CommandType.StoredProcedure);
                LookupCity = task.Read<SuccessResult<AbstractLookupCity>>().SingleOrDefault();
                LookupCity.Item = task.Read<LookupCity>().SingleOrDefault();
            }

            return LookupCity;
        }

        public override PagedList<AbstractLookupCity> LookupCity_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupCity> LookupCity = new PagedList<AbstractLookupCity>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupCity_All, param, commandType: CommandType.StoredProcedure);
                LookupCity.Values.AddRange(task.Read<LookupCity>());
                LookupCity.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupCity;
        }




    }
}
