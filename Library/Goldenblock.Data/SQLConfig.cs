﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Goldenblock.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        //#region LookUpServices
        //public const string LookUpServices_All = "LookUpServices_All";
        //public const string LookUpServices_ParentId = "LookUpServices_ParentId";
        //public const string LookUpServices_ById = "LookUpServices_ById";
        //public const string LookUpServices_Upsert = "LookUpServices_Upsert";
        //public const string LookUpServices_ActInact = "LookUpServices_ActInact";
        //public const string LookUpServices_Delete = "LookUpServices_Delete";
        //#endregion

        public const string Admin_Login = "Admin_Login";


        #region LookupUserType
        public const string LookupUserType_All = "LookupUserType_All";
        public const string LookupUserType_Upsert = "LookupUserType_Upsert";
        #endregion

        #region LookupUserStatus
        public const string LookupUserStatus_All = "LookupUserStatus_All";
        public const string LookupUserStatus_Upsert = "LookupUserStatus_Upsert";
        public const string LookUpStatus_All = "LookUpStatus_All";

        #endregion

        #region LookupNotificationType
        public const string LookupNotificationType_All = "LookupNotificationType_All";
        public const string LookupNotificationType_Upsert = "LookupNotificationType_Upsert";
        #endregion

        #region MasterFAQ
        public const string MasterFAQ_All = "MasterFAQ_All";
        public const string MasterFAQ_Upsert = "MasterFAQ_Upsert";
        public const string MasterFAQ_ById = "MasterFAQ_ById";
        #endregion

        #region LookupConstructionPeriod
        public const string LookupConstructionPeriod_All = "LookupConstructionPeriod_All";
        public const string LookupConstructionPeriod_Upsert = "LookupConstructionPeriod_Upsert";
        public const string LookupConstructionPeriod_ById = "LookupConstructionPeriod_ById";
        #endregion

        #region LookupReferenceDate
        public const string LookupReferenceDate_All = "LookupReferenceDate_All";
        public const string LookupReferenceDate_Upsert = "LookupReferenceDate_Upsert";
        public const string LookupReferenceDate_ById = "LookupReferenceDate_ById";
        #endregion

        #region LookupPropertyStatus
        public const string LookupPropertyStatus_All = "LookupPropertyStatus_All";
        public const string LookupPropertyStatus_Upsert = "LookupPropertyStatus_Upsert";
        #endregion

        #region LookupCountry
        public const string LookupCountry_All = "LookupCountry_All";
        public const string LookupCountry_Upsert = "LookupCountry_Upsert";
        public const string LookupCountry_ById = "LookupCountry_ById";
        #endregion

        #region LookupState
        public const string LookupState_All = "LookupState_All";
        public const string LookupState_Upsert = "LookupState_Upsert";
        public const string LookupState_ById = "LookupState_ById";
        #endregion

        #region LookupCity
        public const string LookupCity_All = "LookupCity_All";
        public const string LookupCity_Upsert = "LookupCity_Upsert";
        public const string LookupCity_ById = "LookupCity_ById";
        #endregion

        #region UserVerificationCode
        public const string UserVerificationCode_All = "UserVerificationCode_All";
        public const string UserVerificationCode_Upsert = "UserVerificationCode_Upsert";
        public const string UserVerificationCode_SendVerificationCode = "UserVerificationCode_SendVerificationCode";
        public const string UserVerificationCode_ById = "UserVerificationCode_ById";
        #endregion

        #region UserQuestions
        public const string UserQuestions_All = "UserQuestions_All";
        public const string UserQuestions_Upsert = "UserQuestions_Upsert";
        public const string UserQuestions_ById = "UserQuestions_ById";
        #endregion

        #region UserNotification
        public const string UserNotification_All = "UserNotification_All";
        public const string UserNotification_ByUserId = "UserNotification_ByUserId";
        public const string UserNotification_Upsert = "UserNotification_Upsert";
        public const string UserNotification_ById = "UserNotification_ById";
        #endregion

        #region Users
        public const string Users_All = "Users_All";
        public const string Users_ById = "Users_ById";
        public const string Users_IsAgreeTermsAndConditions = "Users_IsAgreeTermsAndConditions";
        public const string Users_IsEmailVerified = "Users_IsEmailVerified";
        public const string Users_Login = "Users_Login";
        public const string Users_UpdateLookupStatusId = "Users_UpdateLookupStatusId";
        public const string Users_ChangePassword = "Users_ChangePassword";
        public const string Users_Upsert = "Users_Upsert";
        public const string Users_Logout = "Users_Logout";
        public const string Users_Delete = "Users_Delete";
        public const string SendResetPasswordLinkBy_UserEmail = "SendResetPasswordLinkBy_UserEmail";
        public const string ResetPasswordBy_UserEmail = "ResetPasswordBy_UserEmail";
        #endregion

        #region PropertyFrequentlyAskedQuestions
        public const string PropertyFrequentlyAskedQuestions_All = "PropertyFrequentlyAskedQuestions_All";
        public const string PropertyFrequentlyAskedQuestions_ById = "PropertyFrequentlyAskedQuestions_ById";
        public const string PropertyFrequentlyAskedQuestions_PropertyId = "PropertyFrequentlyAskedQuestions_PropertyId";
        public const string PropertyFrequentlyAskedQuestions_Upsert = "PropertyFrequentlyAskedQuestions_Upsert";
        public const string PropertyFrequentlyAskedQuestions_Delete = "PropertyFrequentlyAskedQuestions_Delete";
        #endregion

        #region PropertyThingsToConsiderWhenInvesting
        public const string PropertyThingsToConsiderWhenInvesting_All = "PropertyThingsToConsiderWhenInvesting_All";
        public const string PropertyThingsToConsiderWhenInvesting_ById = "PropertyThingsToConsiderWhenInvesting_ById";
        public const string PropertyThingsToConsiderWhenInvesting_PropertyId = "PropertyThingsToConsiderWhenInvesting_PropertyId";
        public const string PropertyThingsToConsiderWhenInvesting_Upsert = "PropertyThingsToConsiderWhenInvesting_Upsert";
        public const string PropertyThingsToConsiderWhenInvesting_Delete = "PropertyThingsToConsiderWhenInvesting_Delete";
        #endregion

        #region UserProperties
        public const string UserProperties_All = "UserProperties_All";
        public const string UserProperties_ById = "UserProperties_ById";
        public const string UserProperties_ByPropertyId = "UserProperties_ByPropertyId";
        public const string UserProperties_ByUserId = "UserProperties_ByUserId";
        public const string UserProperties_Upsert = "UserProperties_Upsert";
        #endregion

        #region Property
        public const string Property_All = "Property_All";
        public const string Property_ById = "Property_ById";
        public const string Property_UpdateLookupStatusId = "Property_UpdateLookupStatusId";
        public const string Property_Upsert = "Property_Upsert";
        public const string Property_Delete = "Property_Delete";
        public const string Property_ByUserId = "Property_ByUserId";
        #endregion


    }
}
