﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Rushkar">
//     Copyright Rushkar Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Autofac;
using Goldenblock.Data.Contract;

namespace Goldenblock.Data
{
    

    //using Goldenblock.Data.Contract;

    //using Goldenblock.Data.Contract;


    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<V1.LookUpServicesDao>().As<AbstractLookUpServicesDao>().InstancePerDependency();
            
            builder.RegisterType<V1.LookupUserTypeDao>().As<AbstractLookupUserTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupUserStatusDao>().As<AbstractLookupUserStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupNotificationTypeDao>().As<AbstractLookupNotificationTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupConstructionPeriodDao>().As<AbstractLookupConstructionPeriodDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupCountryDao>().As<AbstractLookupCountryDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterFAQDao>().As<AbstractMasterFAQDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupStateDao>().As<AbstractLookupStateDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupCityDao>().As<AbstractLookupCityDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupPropertyStatusDao>().As<AbstractLookupPropertyStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupReferenceDateDao>().As<AbstractLookupReferenceDateDao>().InstancePerDependency();
            builder.RegisterType<V1.UserVerificationCodeDao>().As<AbstractUserVerificationCodeDao>().InstancePerDependency();
            builder.RegisterType<V1.UserQuestionsDao>().As<AbstractUserQuestionsDao>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationDao>().As<AbstractUserNotificationDao>().InstancePerDependency();
            builder.RegisterType<V1.UsersDao>().As<AbstractUsersDao>().InstancePerDependency();
            builder.RegisterType<V1.PropertyFrequentlyAskedQuestionsDao>().As<AbstractPropertyFrequentlyAskedQuestionsDao>().InstancePerDependency();
            builder.RegisterType<V1.PropertyThingsToConsiderWhenInvestingDao>().As<AbstractPropertyThingsToConsiderWhenInvestingDao>().InstancePerDependency();
            builder.RegisterType<V1.UserPropertiesDao>().As<AbstractUserPropertiesDao>().InstancePerDependency();
            builder.RegisterType<V1.PropertyDao>().As<AbstractPropertyDao>().InstancePerDependency();
            builder.RegisterType<V1.AdminDao>().As<AbstractAdminDao>().InstancePerDependency();




            base.Load(builder);
        }
    }
}
